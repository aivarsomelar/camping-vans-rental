<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Van extends Model
{
    use HasFactory;

    public function images(): HasMany
    {
        return $this->hasMany(Image::class);
    }

    public function attributes(): HasMany
    {
        return $this->hasMany(VanAttribute::class);
    }

    public function prices(): HasMany
    {
        return $this->hasMany(Price::class);
    }

    public function bookings(): HasMany
    {
        return $this->hasMany(Booking::class);
    }

    public function attatchNewPoerpty($attriputeId, $value)
    {
        $this->attributes()->create([
            'attribute_id' => $attriputeId,
            'value' => $value,
        ]);
    }

    public function attatchNewPrice($price, $days, $periodStart, $periodEnd)
    {
        $this->prices()->create([
            'price' => $price,
            'days' => $days,
            'period_start' => $periodStart,
            'period_end' => $periodEnd,
        ]);
    }

    public function attatchNewBooking($start, $end)
    {
        $this->bookings()->create([
            'start' => new Carbon($start),
            'end' => new Carbon($end),
        ]);
    }

    public static function boot()
    {
        parent::boot();
        self::deleting(function ($van) {
            $van->images()->each(function ($image) {
                $image->delete();
            });

            $van->attributes()->each(function ($attripute) {
                $attripute->delete();
            });
            $van->prices()->each(function ($price) {
                $price->delete();
            });
        });
    }
}
