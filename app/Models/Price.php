<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Price extends Model
{
    use HasFactory;

    protected $fillable = ['van_id', 'price', 'days', 'period_start', 'period_end'];

    public function van(): BelongsTo
    {
        return $this->belongsTo(Van::class);
    }
}
