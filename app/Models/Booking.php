<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    use HasFactory;

    protected $fillable = ['van_id', 'customer_id', 'end', 'start', 'customer_name', 'customer_email', 'customer_phone'];

    public function van()
    {
        return $this->belongsTo(Van::class);
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }
}
