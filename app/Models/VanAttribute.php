<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class VanAttribute extends Model
{
    use HasFactory;

    protected $fillable = ['van_id', 'attribute_id', 'value'];

    public function van(): BelongsTo
    {
        return $this->belongsTo(Van::class);
    }

    public function attribute(): BelongsTo
    {
        return $this->belongsTo(Attribute::class);
    }
}
