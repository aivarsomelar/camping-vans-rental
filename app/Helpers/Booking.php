<?php

namespace App\Helpers;

use Carbon\Carbon;

class Booking
{
    public static function generateBookedDates($bookings)
    {
        $disabledDates = [];
        foreach ($bookings as $booking) {
            $selectedDate = new Carbon($booking->start);
            while (new Carbon($booking->end) >= $selectedDate) {
                $disabledDates[] = clone $selectedDate;
                $selectedDate->addDay();
            }
        }

        return $disabledDates;
    }
}
