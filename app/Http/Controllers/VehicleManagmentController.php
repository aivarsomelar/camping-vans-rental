<?php

namespace App\Http\Controllers;

use App\Models\Van;
use Exception;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class VehicleManagmentController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(): View
    {
        return view('admin.vehicles.index', ['vehicles' => Van::all()]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): View
    {
        $vehicle = new Van();

        return view('admin.vehicles.create', ['vehicle' => $vehicle]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Van  $Van
     */
    public function show(Van $vehicleManagment): View
    {
        return view('admin.vehicles.show', ['vehicle' => $vehicleManagment]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Van $Van)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Van $Van)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Van  $van
     */
    public function destroy(Van $vehicleManagment): RedirectResponse
    {
        try {
            $vehicleManagment->delete();

            return redirect()->route('vehicleManagment.index');
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }
}
