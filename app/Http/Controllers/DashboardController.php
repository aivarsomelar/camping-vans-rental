<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use Illuminate\View\View;

class DashboardController extends Controller
{
    public function index(): View
    {
        $bookings = Booking::where('is_ended', false)->get();

        return view('admin/dashboard',
            [
                'newBookings' => $bookings->where('is_new', true),
                'confirmed' => $bookings->where('is_confirmed', true),
                'waiting' => $bookings->where('is_confirmed', false)->where('is_new', false),
            ]
        );
    }
}
