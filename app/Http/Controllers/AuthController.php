<?php

namespace App\Http\Controllers;

use App\Models\Invitation;
use App\Models\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    public function registerForm(Request $request)
    {
        $token = $request->token;
        if (isset($token)) {
            $invitation = Invitation::where('token', $token)->first();
            if ($invitation and Carbon::now()->lessThan($invitation->created_at->addDay())) {
                return view('auth.register', ['invitation' => $invitation]);
            }
        }

        return abort(401);
    }

    public function sotreUser(Request $request): RedirectResponse
    {
        Validator::make($request->toArray(), [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'confirmed'],
            'hash' => ['required', 'string'],
        ])->validate();

        try {
            $user = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => Hash::make($request->password),
            ]);

            $user->assignRole('worker');

            Invitation::where('token', $request->hash)->first()->delete();

            return redirect('dashboard');
        } catch (Exception $exception) {
            throw new Exception($exception->getMessage());
        }
    }
}
