<?php

namespace App\Http\Controllers;

use App\Models\Van;
use Carbon\Carbon;
use Illuminate\View\View;

class VansController extends Controller
{
    public function index(): View
    {
        return view('welcome', ['vehicles' => Van::all(), 'now' => Carbon::now()]);
    }

    public function show(Van $van): View
    {
        $now = Carbon::today();
        $prices = $van->prices->where('period_start', '<=', $now)->where('period_end', '>=', $now);
        $smallestPrice = $prices->min('price');

        return view('vehicle', ['vehicle' => $van, 'prices' => $prices, 'smallestPrice' => $smallestPrice]);
    }
}
