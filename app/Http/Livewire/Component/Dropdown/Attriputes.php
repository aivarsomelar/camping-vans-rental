<?php

namespace App\Http\Livewire\Component\Dropdown;

use App\Models\Attribute;
use Livewire\Component;

class Attriputes extends Component
{
    /**
     * Hold search keyword
     *
     * @var string
     */
    public $keyword;

    public $searchResults;

    public $selectAction;

    /**
     * @param  string  $selectAction - event name what will be triggered when user selects attripute from dropdown. Usuall used to send selecetd data to parent component
     */
    public function mount(string $selectAction): void
    {
        $this->selectAction = $selectAction;
    }

    public function render()
    {
        return view('livewire.component.dropdown.attriputes');
    }

    public function triggerSearch()
    {
        $this->searchResults = null;
        $this->searchResults = Attribute::where('label', 'like', '%'.$this->keyword.'%')->get();
    }

    public function select($attripute)
    {
        $this->keyword = $attripute['label'];
        $this->emit($this->selectAction, $attripute);
        $this->searchResults = null;
    }
}
