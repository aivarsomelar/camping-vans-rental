<?php

namespace App\Http\Livewire\Admin\Users\Create;

use App\Mail\UserInvitation;
use App\Models\Invitation;
use Exception;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Livewire\Component;

class Invite extends Component
{
    public $name;

    public $email;

    public $success;

    public $error;

    protected $rules = [
        'name' => 'required|string',
        'email' => 'required|email',
    ];

    public function render()
    {
        return view('livewire.admin.users.create.invite');
    }

    public function sendInvite()
    {
        $this->error = null;
        $this->success = null;
        $this->validate();

        try {
            $token = Str::random(50);

            Invitation::create([
                'name' => $this->name,
                'email' => $this->email,
                'token' => $token,
            ]);

            Mail::to($this->email)->send(new UserInvitation($token));
            $this->success = true;
            $this->name = null;
            $this->email = null;
        } catch (Exception $exception) {
            $this->error = __('Somehing went wrong. Check logs or try again later');
            Log::alert($exception->getMessage());
        }
    }
}
