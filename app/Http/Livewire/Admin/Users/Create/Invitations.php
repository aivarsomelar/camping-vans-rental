<?php

namespace App\Http\Livewire\Admin\Users\Create;

use App\Models\Invitation;
use Livewire\Component;

class Invitations extends Component
{
    public $invitations;

    public function render()
    {
        $this->invitations = Invitation::all();

        return view('livewire.admin.users.create.invitations');
    }

    public function delete($id)
    {
        Invitation::find($id)->delete();
    }
}
