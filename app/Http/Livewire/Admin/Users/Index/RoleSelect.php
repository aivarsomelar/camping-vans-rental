<?php

namespace App\Http\Livewire\Admin\Users\Index;

use App\Models\User;
use Livewire\Component;
use Spatie\Permission\Models\Role;

class RoleSelect extends Component
{
    public $user;

    public $roles;

    public $userRole;

    public function mount(User $user)
    {
        $this->user = $user;
        $this->userRole = $user->getRoleNames()->first();
        $this->roles = Role::all();
    }

    public function render()
    {
        return view('livewire.admin.users.index.role-select');
    }

    public function select()
    {
        $this->user->assignRole($this->userRole);
    }
}
