<?php

namespace App\Http\Livewire\Admin\Vehicle\Manage\Booking;

use App\Helpers\Booking;
use App\Models\Booking as ModelsBooking;
use App\Models\Customer;
use App\Models\Van;
use Carbon\Carbon;
use Livewire\Component;

class Index extends Component
{
    public $vehicle;

    public $bookings;

    public $show;

    public $addMode;

    public $startDate;

    public $endDate;

    public $name;

    public $email;

    public $phone;

    public $bookedDates = [];

    protected $listeners = [
        'booking.add.hide' => 'hideAddMode',
        'booking.reload' => 'render',
    ];

    protected $rules = [
        'startDate' => 'required|date',
        'endDate' => 'required|date|after:startDate',
        'name' => 'required|string',
        'email' => 'required|string',
        'phone' => 'integer|nullable',
    ];

    protected $messages = [
        'startDate.required' => 'Start date is mandatory',
        'endDate.required' => 'End date is mandatory',
        'name.required' => 'Name is mandatory',
        'email.required' => 'Email is mandatory',
        'startDate.date' => 'Please select date for period start',
        'endDate.date' => 'Please select date for period end',
        'endDate.after' => 'Period end date have to be date after period start date',
        'phone.integer' => 'Phone have to be integer',

    ];

    public function mount(Van $vehicle)
    {
        $this->vehicle = $vehicle;
    }

    public function render()
    {
        $this->getBookings();
        $this->setBookedDates();

        return view('livewire.admin.vehicle.manage.booking.index');
    }

    public function toggleShow()
    {
        $this->show = ! $this->show;
    }

    public function showAddMode()
    {
        $this->addMode = true;
    }

    public function hideAddMode()
    {
        $this->addMode = false;
    }

    public function save()
    {
        $this->validate();

        ModelsBooking::create([
            'van_id' => $this->vehicle->id,
            'customer_id' => null,
            'customer_name' => $this->name,
            'customer_email' => $this->email,
            'customer_phone' => $this->phone,
            'start' => new Carbon($this->startDate),
            'end' => new Carbon($this->endDate),
        ]);

        $this->name = null;
        $this->phone = null;
        $this->email = null;
        $this->startDate = null;
        $this->endDate = null;

        $this->getBookings();
        $this->hideAddMode();
    }

    protected function getBookings()
    {
        $this->vehicle->refresh();
        $this->bookings = $this->vehicle->bookings->where('is_ended', false);
    }

    protected function setBookedDates()
    {
        $this->bookedDates = json_encode(Booking::generateBookedDates($this->bookings));
    }
}
