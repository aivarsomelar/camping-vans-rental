<?php

namespace App\Http\Livewire\Admin\Vehicle\Manage\Booking;

use App\Models\Booking;
use App\Models\Van;
use Carbon\Carbon;
use Exception;
use Livewire\Component;

class Item extends Component
{
    public Booking $booking;

    public $deleteMode;

    public $editMode;

    public $startDate;

    public $endDate;

    public $customer_name;

    public $customer_email;

    public $customer_phone;

    public $forceReload;

    protected $listeners = [
        'booking.item.delete.hide' => 'hideDeleteMode',
        'booking.item.edit.hide' => 'hideEditMode',
    ];

    protected $rules = [
        'startDate' => 'required|date',
        'endDate' => 'required|date|after:startDate',
        'customer_name' => 'required|string',
        'customer_email' => 'required|string',
        'customer_phone' => 'integer|nullable',
    ];

    public function mount(Booking $booking, Van $vehicle, bool $forceReload = false)
    {
        $this->booking = $booking;
        $this->vehicle = $vehicle;
        $this->startDate = (new Carbon($this->booking->start))->format('d.m.Y');
        $this->endDate = (new Carbon($this->booking->end))->format('d.m.Y');
        $this->customer_name = $this->booking->customer_name ?? $this->booking->customer->firstname . ' ' . $this->booking->customer->lastname;
        $this->customer_email = $this->booking->customer_email ?? $this->booking->customer->email;
        $this->customer_phone = $this->booking->customer_phone ?? $this->booking->customer->phone;

        $this->forceReload = $forceReload;
    }

    public function render()
    {
        return view('livewire.admin.vehicle.manage.booking.item');
    }

    public function showDeleteMode()
    {
        $this->deleteMode = true;
    }

    public function hideDeleteMode()
    {
        $this->deleteMode = false;
    }

    public function showEditMode()
    {
        $this->editMode = true;
    }

    public function hideEditMode()
    {
        $this->editMode = false;
    }

    public function save()
    {
        $this->validate();

        $this->booking->start = new Carbon($this->startDate);
        $this->booking->end = new Carbon($this->endDate);
        $this->booking->customer_name = $this->customer_name;
        $this->booking->customer_email = $this->customer_email;
        $this->booking->customer_phone = $this->customer_phone;
        $this->booking->save();

        $this->hideEditMode();

        if ($this->forceReload) {
            return redirect(request()->header('Referer'));
        } else {
            $this->emitUp('booking.reload');
        }
    }

    public function delete()
    {
        try {
            $this->booking->delete();
            $this->hideDeleteMode();
            if ($this->forceReload) {
                return redirect(request()->header('Referer'));
            } else {
                $this->emitUp('booking.reload');
            }
        } catch (Exception $exception) {
            throw new Exception($exception->getMessage(), $exception->getCode());
        }
    }
}
