<?php

namespace App\Http\Livewire\Admin\Vehicle\Manage\Booking;

use App\Models\Booking;
use App\Models\Van;
use Carbon\Carbon;
use Livewire\Component;

class Show extends Component
{
    public $vehicle;

    public $booking;

    public $duration;

    public $price; //TODO add new column to booking table and store price in db

    protected $rules = [
        'booking.price' => 'numeric',
    ];

    public function mount(Van $van, Booking $booking)
    {
        $this->vehicle = $van;
        $this->booking = $booking;
        $this->duration = (new Carbon($this->booking->start))->diffInDays(new Carbon($this->booking->end));

        $this->booking->is_new = false;
        $this->booking->save();
    }

    public function render()
    {
        return view('livewire.admin.vehicle.manage.booking.show');
    }

    public function confirm()
    {
        $this->booking->is_confirmed = true;
        $this->booking->save();
        $this->booking->refresh();
    }

    public function end()
    {
        $this->booking->is_ended = true;
        $this->booking->save();
        $this->redirect(route('vehicleManagment.show', $this->vehicle));
    }

    public function updatePrice()
    {
        $this->validate();
        $this->booking->save();
    }
}
