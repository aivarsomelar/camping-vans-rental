<?php

namespace App\Http\Livewire\Admin\Vehicle\Manage\Prices;

use App\Models\Price;
use App\Models\Van;
use Carbon\Carbon;
use Livewire\Component;

class Show extends Component
{
    public $vehicle;

    public $prices;

    public $show;

    public $addMode;

    /* Create a new price  */
    public $startDate;

    public $carbonizedStartDate;

    public $endDate;

    public $carbonizedEndDate;

    public $days;

    public $price;

    protected $listeners = [
        'price.add.hide' => 'hideAddMode',
        'price.refresh' => 'getVehiclePrices',
    ];

    protected $rules = [
        'carbonizedStartDate' => 'required|date',
        'carbonizedEndDate' => 'required|date|after:carbonizedStartDate',
        'days' => 'required|integer',
        'price' => 'required|numeric',
    ];

    protected $messages = [
        'carbonizedStartDate.required' => 'Period start is mandatory',
        'carbonizedEndDate.required' => 'Period end is mandatory',
        'days.required' => 'Days are mandatory',
        'price.required' => 'Price is mandatory',
        'carbonizedStartDate.date' => 'Please select date for period start',
        'carbonizedEndDate.date' => 'Please select date for period end',
        'carbonizedEndDate.after' => 'Period end date have to be date after period start date',
        'days.integer' => 'Days have to be integer',
        'price.numeric' => 'Price have to be float',

    ];

    public function mount(Van $vehicle)
    {
        $this->vehicle = $vehicle;
        $this->getVehiclePrices();
    }

    public function render()
    {
        return view('livewire.admin.vehicle.manage.prices.show');
    }

    public function getVehiclePrices()
    {
        $this->vehicle->refresh();
        $this->prices = $this->vehicle->prices;
    }

    public function toggleShow()
    {
        $this->show = ! $this->show;
    }

    public function showAddMode()
    {
        if ($this->vehicle->id) {
            $this->addMode = true;
        }
    }

    public function hideAddMode()
    {
        $this->addMode = false;
    }

    public function save()
    {
        if ($this->startDate) {
            $this->carbonizedStartDate = new Carbon($this->startDate);
        }
        if ($this->endDate) {
            $this->carbonizedEndDate = new Carbon($this->endDate);
        }
        $this->carbonizedEndDate->hour = 23;
        $this->carbonizedEndDate->minute = 59;
        $this->carbonizedEndDate->second = 59;

        $this->validate();
        $this->vehicle->attatchNewPrice($this->price, $this->days, $this->carbonizedStartDate, $this->carbonizedEndDate);
        $this->hideAddMode();
        $this->getVehiclePrices();
    }
}
