<?php

namespace App\Http\Livewire\Admin\Vehicle\Manage\Prices;

use App\Models\Price;
use Carbon\Carbon;
use Livewire\Component;

class Item extends Component
{
    public Price $price;

    public $editMode;

    public $startDate;

    public $endDate;

    public $deleteConfirmModal;

    public $isPast;

    public $isActive;

    public $isEnding;

    protected $listeners = [
        'price.edit.hide' => 'hideEditMode',
        'price.delete.hide' => 'hideConfirmModel',
    ];

    protected $rules = [
        'price.period_start' => 'required|date',
        'price.period_end' => 'required|date|after:carbonizedStartDate',
        'price.days' => 'required|integer',
        'price.price' => 'required|numeric',
    ];

    public function mount(Price $price)
    {
        $this->price = $price;
        $periodStart = new Carbon($price->period_start);
        $periodEnd = new Carbon($price->period_end);
        $now = Carbon::now();

        if ($now->isAfter($periodEnd)) {
            $this->isPast = true;
        } elseif ($now->isAfter($periodStart) and $now->isBefore($periodEnd)) {
            $this->isActive = true;
            if ($now->diffInDays($periodEnd) < 7) {
                $this->isEnding = true;
            }
        }

        $this->startDate = $periodStart->format('d.m.Y');
        $this->endDate = $periodEnd->format('d.m.Y');
    }

    public function render()
    {
        return view('livewire.admin.vehicle.manage.prices.item');
    }

    public function update()
    {
        if ($this->startDate) {
            $this->price->period_start = new Carbon($this->startDate);
        }
        if ($this->endDate) {
            $this->price->period_end = new Carbon($this->endDate);
        }
        $this->price->period_end->hour = 23;
        $this->price->period_end->minute = 59;
        $this->price->period_end->second = 59;

        $this->price->update();
        $this->hideEditMode();
    }

    public function hideEditMode()
    {
        $this->editMode = false;
    }

    public function showEditMode()
    {
        $this->editMode = true;
    }

    public function showConfirmModel()
    {
        $this->deleteConfirmModal = true;
    }

    public function hideConfirmModel()
    {
        $this->deleteConfirmModal = false;
    }

    public function delete()
    {
        $this->price->delete();
        $this->emitUp('price.refresh');
    }
}
