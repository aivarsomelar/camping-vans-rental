<?php

namespace App\Http\Livewire\Admin\Vehicle\Manage\Image;

use App\Models\Image;
use Livewire\Component;

class Item extends Component
{
    public $image;

    public $deleteConfirmModal = false;

    protected $listeners = ['gallery.image.closeConfirmModel' => 'closeConfirmModel'];

    public function mount(Image $image)
    {
        $this->image = $image;
    }

    public function render()
    {
        return view('livewire.admin.vehicle.manage.image.item');
    }

    public function toggleConfirmModel()
    {
        $this->deleteConfirmModal = ! $this->deleteConfirmModal;
    }

    public function closeConfirmModel()
    {
        $this->deleteConfirmModal = false;
    }

    public function delete()
    {
        $this->image->delete();
        $this->closeConfirmModel();
        $this->emitUp('gallery.reload');
    }
}
