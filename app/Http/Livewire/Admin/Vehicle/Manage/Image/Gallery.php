<?php

namespace App\Http\Livewire\Admin\Vehicle\Manage\Image;

use App\Models\Van;
use Livewire\Component;

class Gallery extends Component
{
    public $vehicle;

    public $images;

    public $show;

    public $upload;

    protected $listeners = [
        'gallery.reload' => 'render',
        'gallery.upload.close' => 'hideUpload',
    ];

    public function mount(Van $vehicle)
    {
        $this->vehicle = $vehicle;
    }

    public function render()
    {
        $this->getImages($this->vehicle);

        return view('livewire.admin.vehicle.manage.image.gallery');
    }

    public function toggleShow()
    {
        $this->show = ! $this->show;
    }

    public function getImages($vehicle)
    {
        $this->images = $vehicle->images;
    }

    public function showUpload()
    {
        if ($this->vehicle->id) {
            $this->upload = true;
        }
    }

    public function hideUpload()
    {
        $this->upload = false;
    }
}
