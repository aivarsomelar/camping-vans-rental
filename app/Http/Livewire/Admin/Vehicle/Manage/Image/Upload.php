<?php

namespace App\Http\Livewire\Admin\Vehicle\Manage\Image;

use App\Models\Image;
use App\Models\Van;
use Livewire\Component;
use Livewire\WithFileUploads;

class Upload extends Component
{
    use WithFileUploads;

    public $photo;

    public $vehicle;

    public $closeEvent;

    public function mount(Van $vehicle, $closeEvent)
    {
        $this->vehicle = $vehicle;
        $this->closeEvent = $closeEvent;
    }

    public function render()
    {
        return view('livewire.admin.vehicle.manage.image.upload');
    }

    public function save()
    {
        $this->validate([
            'photo' => 'image|max:10024', // 10MB Max
        ]);

        $path = 'imgs';
        $upload = $this->photo->storePublicly($path, 'public');

        Image::create(['van_id' => $this->vehicle->id, 'path' => $upload]);
        $this->photo = null;
        $this->emit($this->closeEvent);
    }

    public function close()
    {
        $this->emitUp($this->closeEvent);
    }
}
