<?php

namespace App\Http\Livewire\Admin\Vehicle\Manage\Properties;

use App\Models\VanAttribute;
use Livewire\Component;

class Item extends Component
{
    public VanAttribute $property;

    public $editMode = false;

    protected $listeners = [
        'property.edit.close' => 'closeEdit',
    ];

    protected $rules = [
        'property.value' => 'required|string',
    ];

    public function mount(VanAttribute $property)
    {
        $this->property = $property;
    }

    public function render()
    {
        return view('livewire.admin.vehicle.manage.properties.item');
    }

    public function openEdit()
    {
        $this->editMode = true;
    }

    public function closeEdit()
    {
        $this->editMode = false;
        $this->property->refresh();
    }

    public function deleteVehicelProperty()
    {
        $this->property->delete();
        $this->editMode = false;
        $this->emitUp('properties.refresh');
    }

    public function save()
    {
        $this->property->save();
        $this->closeEdit();
    }
}
