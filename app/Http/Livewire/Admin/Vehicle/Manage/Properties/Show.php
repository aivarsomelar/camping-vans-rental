<?php

namespace App\Http\Livewire\Admin\Vehicle\Manage\Properties;

use App\Models\Attribute;
use App\Models\Van;
use Livewire\Component;

class Show extends Component
{
    public $vehicle;

    public $properties;

    public $show;

    public $addMode;

    /* New vehicle property */
    public $attriputeId;

    public $attriputeValue;

    protected $listeners = [
        'properties.refresh' => 'getVanProperties',
        'properties.add.hide' => 'hideAddProperty',
        'prpperties.setPropertyDataForNewVehicleProperty' => 'setPropertyDataForNewVehicleProperty',
    ];

    protected $rules = [
        'attriputeId' => 'required|integer',
        'attriputeValue' => 'required|string',
    ];

    protected $messages = [
        'attriputeId.required' => 'Please choose property for van',
        'attriputeId.integer' => 'This property can not be selected. Please call developer (error message:"attriputeId must be int").',
        'attriputeValue.required' => 'Please add value for property.',
        'attriputeValue.string' => 'This value can not be used. Please call developer (error message:"attriputeValue must be string").',
    ];

    public function mount(Van $vehicle)
    {
        $this->vehicle = $vehicle;
        $this->getVanProperties();
    }

    public function render()
    {
        return view('livewire.admin.vehicle.manage.properties.show');
    }

    public function toggleShow()
    {
        $this->show = ! $this->show;
    }

    public function getVanProperties()
    {
        $this->vehicle = $this->vehicle->refresh();
        $this->properties = $this->vehicle->attributes;
    }

    public function showAddProperty()
    {
        if ($this->vehicle->id) {
            $this->addMode = true;
        }
    }

    public function hideAddProperty()
    {
        $this->addMode = false;
    }

    public function saveNewVehicleProperty()
    {
        $this->validate();
        $this->vehicle->attatchNewPoerpty($this->attriputeId, $this->attriputeValue);
        $this->attriputeId = null;
        $this->attriputeValue = null;
        $this->getVanProperties();
        $this->render();
        $this->hideAddProperty();
    }

    public function setPropertyDataForNewVehicleProperty(Attribute $property)
    {
        $this->attriputeId = $property->id;
    }
}
