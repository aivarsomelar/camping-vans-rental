<?php

namespace App\Http\Livewire\Admin\Vehicle\Manage;

use App\Models\Van;
use Livewire\Component;

class Info extends Component
{
    public $vehicle;

    public $isNew;

    public $editMode;

    protected $listeners = [
        'info.edit.hide' => 'hideEditMode',
    ];

    protected $rules = [
        'vehicle.name' => 'required|string',
        'vehicle.model' => 'required|string',
    ];

    public function mount(Van $vehicle, ?bool $isNew = false)
    {
        $this->vehicle = $vehicle;
        $this->isNew = $isNew;

        if ($this->isNew and ! $vehicle->id) {
            $this->showEditMode();
        }
    }

    public function render()
    {
        return view('livewire.admin.vehicle.manage.info');
    }

    public function showEditMode()
    {
        $this->editMode = true;
    }

    public function hideEditMode()
    {
        if ($this->vehicle->id) {
            $this->editMode = false;
        } else {
            $this->redirect(route('vehicleManagment.index'));
        }
    }

    public function save()
    {
        $this->vehicle->save();
        $this->hideEditMode();
        $this->redirect(route('vehicleManagment.show', ['vehicleManagment' => $this->vehicle]));
    }
}
