<?php

namespace App\Http\Livewire\Components;

use App\Models\Van;
use Carbon\Carbon;
use Livewire\Component;

class PriceList extends Component
{
    public $vehicle;

    public $periods;

    public $activePeriod;

    public $activePeriodKey;

    public $prices;

    public $smallestPrice;

    public function mount(Van $vehicle)
    {
        $this->vehicle = $vehicle;

        $this->setActivePeriod();
    }

    public function render()
    {
        return view('livewire.components.price-list');
    }

    protected function getAllPeriods()
    {
        $this->periods = $this->vehicle->prices->unique('period_start')->unique('period_end')->sort()->map(function ($item, $key) {
            return [
                'period_start' => $item['period_start'],
                'period_end' => $item['period_end'],
            ];
        })->values();
    }

    public function setActivePeriod(bool $next = null)
    {
        if (! $this->periods) {
            $this->getAllPeriods();
        }

        if (! isset($this->activePeriodKey)) {
            $now = Carbon::today();
            $activePeriod = $this->periods->where('period_start', '<=', $now)->where('period_end', '>=', $now);
            $this->activePeriodKey = $activePeriod->keys()->first();
            $this->activePeriod = $activePeriod->first();
        } else {
            if ($next) {
                $this->activePeriodKey = $this->activePeriodKey + 1;
            } else {
                $this->activePeriodKey = $this->activePeriodKey - 1;
            }

            if ($this->periods->keys()->contains($this->activePeriodKey)) {
                $this->activePeriod = $this->periods[$this->activePeriodKey];
            } else {
                $this->activePeriod = $this->periods[0];
                $this->activePeriodKey = 0;
            }
        }
        $this->prices = $this->vehicle->prices->where('period_start', $this->activePeriod['period_start'])->where('period_end', $this->activePeriod['period_end']);
        $this->smallestPrice = $this->prices->min('price');
    }
}
