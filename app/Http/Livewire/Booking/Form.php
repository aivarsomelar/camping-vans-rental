<?php

namespace App\Http\Livewire\Booking;

use App\Helpers\Booking;
use App\Mail\BookingEmailToAdmin;
use App\Models\Booking as ModelsBooking;
use App\Models\Customer;
use App\Models\Van;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use Livewire\Component;

class Form extends Component
{
    public $name;

    public $phone;

    public $email;

    public $message;

    public $startDate;

    public $startDateString;

    public $endDate;

    public $endDateString;

    public $haveReadRentalConditions = false;

    public $vehicle;

    public $bookedDates;

    public $error;

    public $success;

    protected $rules = [
        'name' => 'required|string',
        'email' => 'required|email',
        'phone' => 'required|integer',
        'startDate' => 'required|date|after:today',
        'endDate' => 'required|date|after:startDate',
        'message' => 'string',
        'haveReadRentalConditions' => 'required|accepted',
    ];

    protected $messages = [
        'endDate.after' => 'Rental period end date must be after start date.',
        'name.regex' => 'Please enter full name.',
    ];

    public function mount(Van $vehicle, string $defaultMessage = null)
    {
        $this->message = ($defaultMessage) ?? __(
            'Hello! I want to book :name :model.',
            ['name' => $vehicle->name, 'model' => $vehicle->model]
        );
        $this->vehicle = $vehicle;
        $this->setBookedDates();
    }

    public function render()
    {
        return view('livewire.booking.form');
    }

    public function sendMessage()
    {
        $this->validate();
        try {
            Mail::to(env('MAIL_FROM_ADDRESS'))
            ->send(
                new BookingEmailToAdmin([
                    'name' => $this->name,
                    'phone' => $this->phone,
                    'email' => $this->email,
                    'message' => $this->message,
                    'startDate' => $this->startDate,
                    'endDate' => $this->endDate,
                    'vehicle' => $this->vehicle->name.' '.$this->vehicle->model,
                    ])
                );
                
            ModelsBooking::create([
                'van_id' => $this->vehicle->id,
                'customer_id' => null,
                'customer_name' => $this->name,
                'customer_email' => $this->email,
                'customer_phone' => $this->phone,
                'start' => new Carbon($this->startDate),
                'end' => new Carbon($this->endDate),
            ]);

            $this->success = true;
        } catch (\Exception $exception) {
            dd($exception);
            $this->error = true;
        }
    }

    protected function setBookedDates()
    {
        $this->bookedDates = json_encode(Booking::generateBookedDates($this->vehicle->bookings));
    }
}
