<?php

namespace App\View\Components;

use Illuminate\View\Component;

class ModalFullScreen extends Component
{
    public string $closeEventName;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(string $closeEventName)
    {
        $this->closeEventName = $closeEventName;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.modal-full-screen');
    }
}
