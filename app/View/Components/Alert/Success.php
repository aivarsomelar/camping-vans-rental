<?php

namespace App\View\Components\Alert;

use Illuminate\View\Component;

class Success extends Component
{
    public string $message;

    public ?string $subMessage;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(string $message, ?string $subMessage = null)
    {
        //
        $this->message = $message;
        $this->subMessage = $subMessage;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.alert.success');
    }
}
