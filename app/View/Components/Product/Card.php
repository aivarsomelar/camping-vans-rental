<?php

namespace App\View\Components\Product;

use Illuminate\View\Component;

class Card extends Component
{
    /**
     * @var string
     */
    public $imgPath;

    /**
     * @var string
     */
    public $title;

    /**
     * @var float
     */
    public $price;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(string $imgPath, string $title, float $price)
    {
        $this->imgPath = $imgPath;
        $this->title = $title;
        $this->price = $price;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.product.card');
    }
}
