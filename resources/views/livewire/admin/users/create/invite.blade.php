<div>
    @if($error)
        <x-alert.error :message="$error"></x-alert.error>
    @elseif($success)
        <x-alert.success message="Invitation sent"></x-alert.success>
    @endif
    <div class="col-span-6 sm:col-span-4">
        <x-jet-label for="name" value="{{ __('Name') }}" />
        <x-jet-input id="name" type="text" class="mt-1 block w-full" wire:model="name" autocomplete="name" />
        <x-jet-input-error for="name" class="my-2" />
    </div>

    <!-- Email -->
    <div class="col-span-6 sm:col-span-4">
        <x-jet-label for="email" value="{{ __('Email') }}" />
        <x-jet-input id="email" type="email" class="mt-1 block w-full" wire:model="email" />
        <x-jet-input-error for="email" class="my-2" />
    </div>
    <div class="col-span-6">
        <button wire:click="sendInvite" wire:loading.remove
            class="bg-gradient-base border border-blue-600 hover:bg-white text-white hover:text-blue-600 font-bold uppercase text-xs mt-5 py-2 px-4 rounded cursor-pointer">
            {{ __('Invite user') }}
        </button>
        <button wire:loading wire:target="sendInvite"
            class="bg-gradient-base border border-blue-600 hover:bg-white text-white hover:text-blue-600 font-bold uppercase text-xs mt-5 py-2 px-4 rounded cursor-pointer">
            {{ __('Please wait') }}
        </button>
    </div>
</div>
