<div>
    <select name="userRole" class="boarder border-gray-300 rounded-lg" wire:model="userRole" wire:change="select">
        @foreach ($this->roles as $role)
            <option value="{{ $role->name }}">{{ $role->name }}</option>
        @endforeach
    </select>
</div>
