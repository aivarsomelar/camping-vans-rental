<div class="py-6 mx-auto max-w-7xl sm:px-6 lg:px-8">
    <div class="p-4 my-2 rounded-md shadow-xl">
        <div class="grid grid-cols-2 gap-1">
            <div>@lang('Vehicle'):</div>
            <a href="{{ route('vehicleManagment.show', $vehicle) }}" class="font-bold">{{ $vehicle->name }} {{ $vehicle->model }}</a>
            <div class="col-span-2 border-b border-gray-300"></div>

            <div>@lang('Customer'):</div>
            @if ($booking->customer_name)
                <div class="font-bold">{{ $booking->customer_name }}</div>
            @else
            <div class="font-bold">{{ $booking->customer->firstname }} {{ $booking->customer->lastname }}</div>
            @endif
            @if ($booking->customer_email)
                <div class="font-bold">{{ $booking->customer_email }}</div>
                @else
                <div class="col-start-2 font-bold">{{ $booking->customer->email }}</div>
            @endif
            @if ($booking->customer_phone)
                <div class="font-bold">{{ $booking->customer_phone }}</div>
                @else
                <div class="col-start-2 font-bold">{{ $booking->customer->phone }}</div>
            @endif
            <div class="col-span-2 border-b border-gray-300"></div>

            <div>@lang('Booking'):</div>
            <div class="font-bold">{{ (new Carbon\Carbon($booking->start))->format('d.m.Y') }} - {{ (new Carbon\Carbon($booking->end))->format('d.m.Y') }}</div>
            <div>@lang('Duration'):</div>
            <div class="font-bold">{{ $duration }}</div>
            <div class="col-span-2 border-b border-gray-300"></div>

            <div class="my-auto">@lang('Price'):</div>
            <div class="font-bold">
                <input type="text" placeholder="{{ __('price') }}" wire:model='booking.price' wire:change='updatePrice'
                                class="w-full h-10 p-2 my-2 leading-tight text-gray-700 border-0 rounded-md shadow-md bg-gray-50 focus:outline-none focus:ring-transparent">
            </div>
        </div>

        <div class="grid w-full my-2 justify-items-end">
            <div>
                <button class="px-4 py-2 font-bold text-white border border-blue-600 rounded bg-gradient-base hover:bg-white hover:text-blue-600">
                    @lang('Save contract')
                </button>
                @if ($booking->is_confirmed)
                    <button class="px-4 py-2 font-bold text-white border border-blue-600 rounded bg-gradient-base hover:bg-white hover:text-blue-600"
                        wire:click="end">
                        @lang('End')
                    </button>
                @else
                    <button class="px-4 py-2 font-bold text-white border border-blue-600 rounded bg-gradient-base hover:bg-white hover:text-blue-600"
                        wire:click="confirm">
                        @lang('Confirm')
                    </button>
                @endif
            </div>
        </div>
    </div>
</div>
