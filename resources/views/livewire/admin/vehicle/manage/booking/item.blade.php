<div class="p-4 my-2 rounded-lg shadow-lg hover:bg-gray-200 lg:my-0">
    <div class="grid grid-cols-5 gap-1">
        <a class="grid grid-cols-4 col-span-4 gap-1" href="{{ route('booking.show', ['van' => $vehicle, 'booking' => $booking]) }}">
            <div class="my-auto">{{ \Carbon\Carbon::create($booking->start)->format('d.m.Y') }} - {{ \Carbon\Carbon::create($booking->end)->format('d.m.Y') }}</div>
            @if ($booking->customer_name)
            <div class="my-auto">{{ $booking->customer_name }}</div>
                @else
                <div class="my-auto">{{ $booking->customer->firstname }} {{ $booking->customer->lastname }}</div>
            @endif
            @if ($booking->customer_email)
                <div class="my-auto">{{ $booking->customer_email }}</div>
                @else
                <div class="my-auto">{{ $booking->customer->email }}</div>
            @endif
            <div class="my-auto justify-items-center">
                @if ($booking->is_confirmed and \Carbon\Carbon::create($booking->start)->lte(\Carbon\Carbon::now()) and \Carbon\Carbon::create($booking->end)->gte(\Carbon\Carbon::now()))
                    <div class="p-2 text-center bg-blue-300 border border-blue-300 rounded-lg">@lang('In drive')</div>
                @elseif ($booking->is_confirmed)
                    <div class="p-2 text-center bg-green-300 border border-green-300 rounded-lg">@lang('Confirmed')</div>
                @elseif ($booking->is_new)
                    <div class="p-2 text-center bg-yellow-400 border border-yellow-400 rounded-lg">@lang('New')</div>
                @else
                    <div class="p-2 text-center bg-purple-400 border border-purple-400 rounded-lg">@lang('Waiting')</div>
                @endif
            </div>
        </a>
        <div class="flex justify-end gap-2">
            <button wire:click='showEditMode'
                class="px-4 py-2 text-xs font-bold text-white uppercase border border-blue-600 rounded cursor-pointer bg-gradient-base hover:bg-white hover:text-blue-600">
                {{ __('Edit') }}
            </button>
            <button wire:click='showDeleteMode'
                class="px-4 py-2 text-xs font-bold text-white uppercase bg-red-600 border border-red-600 rounded cursor-pointer hover:bg-red-800 hover:border-red-800">
                {{ __('Delite') }}
            </button>
        </div>
    </div>
    @if ($editMode)
            <x-modal-full-screen closeEventName="booking.item.edit.hide">
                <x-slot name="title">@lang('Add booking')</x-slot>
                <x-slot name="body">
                    @if ($errors->any())
                        <div class="p-6 bg-red-300 border border-red-400 rounded-lg">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ __($error) }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="grid items-start grid-cols-2 gap-4 my-2">
                        <div class="flex items-center rounded-md bg-gray-50">
                            <x-input.date wire:model="startDate" :error="$errors->first('startDate')" placeholder="booking start date" />
                        </div>
                        <div class="flex items-center rounded-md bg-gray-50">
                            <x-input.date wire:model="endDate" :error="$errors->first('endDate')" placeholder="booking end date" />
                        </div>

                        <div class="flex items-center rounded-md bg-gray-50">
                            <input type="text" placeholder="{{ __('customer firstname') }}" wire:model='customer_name'
                                class="w-full h-10 px-2 py-2 leading-tight text-gray-700 border-0 rounded-md shadow-md bg-gray-50 focus:outline-none focus:ring-transparent">
                        </div>

                        <div class="flex items-center rounded-md bg-gray-50">
                            <input type="text" placeholder="{{ __('customer email') }}" wire:model='customer_email'
                                class="w-full h-10 px-2 py-2 leading-tight text-gray-700 border-0 rounded-md shadow-md bg-gray-50 focus:outline-none focus:ring-transparent">
                        </div>
                        <div class="flex items-center rounded-md bg-gray-50">
                            <input type="text" placeholder="{{ __('customer phone') }}" wire:model='customer_phone'
                                class="w-full h-10 px-2 py-2 leading-tight text-gray-700 border-0 rounded-md shadow-md bg-gray-50 focus:outline-none focus:ring-transparent">
                        </div>
                    </div>
                </x-slot>
                <x-slot name="buttons">
                    <button
                        class="px-4 py-2 font-bold text-white border border-blue-600 rounded bg-gradient-base hover:bg-white hover:text-blue-600"
                        wire:click='save'>
                        @lang('Save')
                    </button>
                    <button class="px-4 py-2 font-semibold bg-gray-400 rounded hover:bg-gray-700 hover:text-white"
                        wire:click='hideEditMode'>
                        @lang('Close')
                    </button>
                </x-slot>
            </x-modal-full-screen>
            @endif
    @if ($deleteMode)
        <x-modal-full-screen closeEventName="booking.item.delete.hide">
            <x-slot name="title">{{ __('Delete booking?') }}</x-slot>
            <x-slot name="body">{{ __('Are you sure you want to delete this booking?') }}</x-slot>
            <x-slot name="buttons">
                <button class="px-4 py-2 font-bold text-white bg-red-500 rounded hover:bg-red-700" wire:click='delete'>
                    @lang('Yes')
                </button>
                <button class="px-4 py-2 font-semibold bg-gray-400 rounded hover:bg-gray-700 hover:text-white" wire:click='hideDeleteMode'>
                    @lang('No')
                </button>
            </x-slot>
        </x-modal-full-screen>
    @endif
</div>
