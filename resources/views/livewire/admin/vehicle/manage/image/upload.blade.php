<div>
    <x-modal-full-screen :closeEventName="$closeEvent">
        <x-slot name="title">@lang('Image upload')</x-slot>
        <x-slot name="body">
            <section class="h-full overflow-auto p-8 w-full">
            <div class="border-dashed border-2 border-gray-400 py-12 flex flex-col justify-center items-center w-full">
                @if ($photo)
                    <img src="{{ $photo->temporaryUrl() }}">
                @else
                    <input type="file" wire:model="photo" class="mt-2 rounded-lg px-3 py-1 bg-gray-200 hover:bg-gray-300 focus:shadow-outline focus:outline-none">
                    @error('photo') <span class="text-center text-red-700">{{ $message }}</span> @enderror
                @endif

            </div>
            </section>
        </x-slot>
        <x-slot name="buttons">
            <button class="bg-gradient-base border border-blue-600 hover:bg-white text-white hover:text-blue-600 font-bold py-2 px-4 rounded" wire:click='save'>
                @lang('Save')
            </button>
            <button class="bg-gray-400 hover:bg-gray-700 font-semibold hover:text-white py-2 px-4 rounded" wire:click='close'>
                @lang('Close')
            </button>
        </x-slot>
    </x-modal-full-screen>
</div>
