<div class="relative w-full h-full flex items-center hover-trigger">
    <img class="w-full" src="{{ Storage::url($image->path) }}">
    <div class="bg-black opacity-60 text-gray-400 absolute top-0 left-0 w-full h-full items-center place-content-center hover-target">
        <svg xmlns="http://www.w3.org/2000/svg" class="h-10 w-10 cursor-pointer" fill="none" viewBox="0 0 24 24" stroke="currentColor" wire:click="toggleConfirmModel">
            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16" />
          </svg>
    </div>
    @if ($deleteConfirmModal)
        <x-modal-full-screen closeEventName="gallery.image.closeConfirmModel">
            <x-slot name="title">{{ __('Delete image?') }}</x-slot>
            <x-slot name="body">{{ __('Are you sure you want to delete this image?') }}</x-slot>
            <x-slot name="buttons">
                <button class="bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 rounded" wire:click='delete'>
                    @lang('Yes')
                </button>
                <button class="bg-gray-400 hover:bg-gray-700 font-semibold hover:text-white py-2 px-4 rounded" wire:click='closeConfirmModel'>
                    @lang('No')
                </button>
            </x-slot>
        </x-modal-full-screen>
    @endif
</div>
