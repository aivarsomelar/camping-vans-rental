<div class="shadow-xl rounded-md my-2">
    <div class="grid grid-cols-1 rounded-md shadow-xl text-xl font-semibold text-gray-800 p-4 border border-gray-100 cursor-pointer" wire:click='toggleShow'>
        <h3>{{ __('Mange images') }}</h3>
    </div>
    @if ($show)
        <div class="flex justify-end m-2 mb-6">
            <button wire:click='showUpload'
                class="border font-bold uppercase text-xs mt-5 py-2 px-4 rounded cursor-pointer
                @if ($vehicle->id)
                    bg-gradient-base border-blue-600 hover:bg-white text-white hover:text-blue-600
                @else
                    bg-gray-600 border-gray-600 hover:bg-gray-500 text-white
                @endif">
                {{ __('Upload image') }}
            </button>
        </div>
        <div class="grid grid-cols-1 gap-4 @if ($images->count() > 6) lg:grid-cols-6 @else lg:grid-cols-{{ $images->count() }} @endif items-center pb-4">
            @foreach ($images as $image)
            <livewire:admin.vehicle.manage.image.item :image="$image" :key="'image' . $image->id . '_' . time()">
            @endforeach
        </div>
    @endif

    @if($upload)
        <div>
            <livewire:admin.vehicle.manage.image.upload :vehicle="$vehicle" closeEvent="gallery.upload.close">
        </div>
    @endif

    <style>
        .hover-trigger .hover-target {
            display: none;
        }

        .hover-trigger:hover .hover-target {
            display: flex;
        }
    </style>
</div>
