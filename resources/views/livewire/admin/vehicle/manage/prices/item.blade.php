<div class="rounded-lg shadow-lg p-4 hover:bg-gray-200 my-2 lg:my-0
    @if ($isPast) bg-gray-700 hover:bg-gray-600
    @elseif ($isEnding) bg-orange-200 hover:bg-orange-300
    @elseif ($isActive) bg-sky-200 hover:bg-sky-300
    @endif">
    {{-- Mobile view start --}}
    <div class="lg:hidden">
        <div class="grid grid-cols-2">
            <div>@lang('Period'):</div>
            <div>{{ \Carbon\Carbon::create($price->period_start)->format('d.m.Y') }} - {{ \Carbon\Carbon::create($price->period_end)->format('d.m.Y') }}</div>
        </div>
        <div class="grid grid-cols-2">
            <div>@lang('Starting from (days)'):</div>
            <div>{{ $price->days }}</div>
        </div>
        <div class="grid grid-cols-2">
            <div>@lang('Price'):</div>
            <div>{{ $price->price }}€</div>
        </div>
        <div class="flex gap-2 justify-end">
            <button
                class="bg-gradient-base border border-blue-600 hover:bg-white text-white hover:text-blue-600 font-bold uppercase text-xs mt-5 py-2 px-4 rounded cursor-pointer">
                {{ __('Edit') }}
            </button>
            <button wire:click='showConfirmModel'
                class="bg-red-600 border border-red-600 hover:bg-red-800 hover:border-red-800 text-white font-bold uppercase text-xs mt-5 py-2 px-4 rounded cursor-pointer">
                {{ __('Delite') }}
            </button>
        </div>
    </div>
    {{-- Mobile view end --}}
    {{-- Desktop view start --}}
    <div class="hidden lg:grid grid-cols-4 gap-6">
        <div class="my-auto">{{ \Carbon\Carbon::create($price->period_start)->format('d.m.Y') }} - {{ \Carbon\Carbon::create($price->period_end)->format('d.m.Y') }}</div>
        <div class="text-center my-auto">{{ $price->days }}</div>
        <div class="text-center my-auto">{{ $price->price }}€</div>
        <div class="flex gap-2 justify-end content-start">
            <button wire:click='showEditMode'
                class="bg-gradient-base border border-blue-600 hover:bg-white text-white hover:text-blue-600 font-bold uppercase text-xs py-2 px-4 rounded cursor-pointer">
                {{ __('Edit') }}
            </button>
            <button wire:click='showConfirmModel'
                class="bg-red-600 border border-red-600 hover:bg-red-800 hover:border-red-800 text-white font-bold uppercase text-xs py-2 px-4 rounded cursor-pointer">
                {{ __('Delite') }}
            </button>
        </div>
    </div>
    {{-- Desktop view end --}}
    @if ($editMode)
        <x-modal-full-screen closeEventName="price.edit.hide">
            <x-slot name="title">@lang('Edit vehicle price')</x-slot>
            <x-slot name="body">
                @if ($errors->any())
                    <div class="border border-red-400 bg-red-300 rounded-lg p-6">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ __($error) }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="grid grid-cols-4 gap-4 items-start my-2">
                    <div class="flex items-center bg-gray-50 rounded-md">
                            <x-input.date wire:model="startDate" :error="$errors->first('price.period_start')" placeholder="period date" />
                    </div>
                    <div class="flex items-center bg-gray-50 rounded-md">
                        <x-input.date wire:model="endDate" :error="$errors->first('price.period_end')" placeholder="period end" />
                    </div>
                    <div class="flex items-center bg-gray-50 rounded-md">
                        <input type="text" placeholder="{{ __('days') }}" wire:model='price.days'
                            class="w-full rounded-md bg-gray-50 text-gray-700 leading-tight focus:outline-none py-2 px-2 border-0 focus:ring-transparent h-10 shadow-md">
                    </div>
                    <div class="flex items-center bg-gray-50 rounded-md">
                        <input type="text" placeholder="{{ __('price') }}" wire:model='price.price'
                            class="w-full rounded-md bg-gray-50 text-gray-700 leading-tight focus:outline-none py-2 px-2 border-0 focus:ring-transparent h-10 shadow-md">
                    </div>
                </div>
            </x-slot>
            <x-slot name="buttons">
                <button
                    class="bg-gradient-base border border-blue-600 hover:bg-white text-white hover:text-blue-600 font-bold py-2 px-4 rounded"
                    wire:click='update'>
                    @lang('Update')
                </button>
                <button class="bg-gray-400 hover:bg-gray-700 font-semibold hover:text-white py-2 px-4 rounded"
                    wire:click='hideEditMode'>
                    @lang('Close')
                </button>
            </x-slot>
        </x-modal-full-screen>
    @endif
    @if ($deleteConfirmModal)
        <x-modal-full-screen closeEventName="price.delete.hide">
            <x-slot name="title">{{ __('Delete price?') }}</x-slot>
            <x-slot name="body">{{ __('Are you sure you want to delete this price?') }}</x-slot>
            <x-slot name="buttons">
                <button class="bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 rounded" wire:click='delete'>
                    @lang('Yes')
                </button>
                <button class="bg-gray-400 hover:bg-gray-700 font-semibold hover:text-white py-2 px-4 rounded" wire:click='hideConfirmModel'>
                    @lang('No')
                </button>
            </x-slot>
        </x-modal-full-screen>
    @endif
</div>
