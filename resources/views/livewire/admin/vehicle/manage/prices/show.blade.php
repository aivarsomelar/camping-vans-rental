<div class="shadow-xl rounded-md my-2">
    <div class="grid grid-cols-1 rounded-md shadow-xl text-xl font-semibold text-gray-800 p-4 border border-gray-100 cursor-pointer"
        wire:click='toggleShow'>
        <h3>{{ __('Mange prices') }}</h3>
    </div>

    @if ($show)
        <div class="flex justify-end m-2 mb-6">
            <button wire:click='showAddMode'
                class="border font-bold uppercase text-xs mt-5 py-2 px-4 rounded cursor-pointer
                    @if ($vehicle->id)
                        bg-gradient-base border-blue-600 hover:bg-white text-white hover:text-blue-600
                    @else
                        bg-gray-600 border-gray-600 hover:bg-gray-500 text-white
                    @endif">
                {{ __('Add Price') }}
            </button>
            @if ($addMode)
            <x-modal-full-screen closeEventName="price.add.hide">
                <x-slot name="title">@lang('Add vehicle price')</x-slot>
                <x-slot name="body">
                    @if ($errors->any())
                        <div class="border border-red-400 bg-red-300 rounded-lg p-6">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ __($error) }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="grid grid-cols-4 gap-4 items-start my-2">
                        <div class="flex items-center bg-gray-50 rounded-md">
                            <x-input.date wire:model="startDate" :error="$errors->first('startDate')" placeholder="period date" />
                        </div>
                        <div class="flex items-center bg-gray-50 rounded-md">
                            <x-input.date wire:model="endDate" :error="$errors->first('endDate')" placeholder="period end" />
                        </div>
                        <div class="flex items-center bg-gray-50 rounded-md">
                            <input type="text" placeholder="{{ __('days') }}" wire:model='days'
                                class="w-full rounded-md bg-gray-50 text-gray-700 leading-tight focus:outline-none py-2 px-2 border-0 focus:ring-transparent h-10 shadow-md">
                        </div>
                        <div class="flex items-center bg-gray-50 rounded-md">
                            <input type="text" placeholder="{{ __('price') }}" wire:model='price'
                                class="w-full rounded-md bg-gray-50 text-gray-700 leading-tight focus:outline-none py-2 px-2 border-0 focus:ring-transparent h-10 shadow-md">
                        </div>
                    </div>
                </x-slot>
                <x-slot name="buttons">
                    <button
                        class="bg-gradient-base border border-blue-600 hover:bg-white text-white hover:text-blue-600 font-bold py-2 px-4 rounded"
                        wire:click='save'>
                        @lang('Save')
                    </button>
                    <button class="bg-gray-400 hover:bg-gray-700 font-semibold hover:text-white py-2 px-4 rounded"
                        wire:click='hideAddMode'>
                        @lang('Close')
                    </button>
                </x-slot>
            </x-modal-full-screen>
            @endif
        </div>
        <div class="grid grid-cols-1 lg:grid-row-1 gap-2 lg:gap-1 p-4">
            {{-- "table" header for desktop start --}}
            <div class="hidden lg:grid grid-cols-4 gap-6 px-4">
                <div>@lang('Period')</div>
                <div class="text-center">@lang('Starting from (days)')</div>
                <div class="text-center">@lang('Price')</div>
                <div class="text-right">@lang('Actions')</div>
            </div>
            {{-- "table" header for desktop end --}}
            @foreach ($prices as $price)
                <livewire:admin.vehicle.manage.prices.item :price="$price" :key="'price_' .$price->id . time()" />
            @endforeach
        </div>
    @endif
</div>
