<div>
    <div class="grid grid-cols-2 border-b border-gray-300 rounded-md hover:border hover:bg-gray-300 p-2 cursor-pointer" wire:click='openEdit'>
        <div>{{ $property->attribute->label }}:</div>
        <div class="text-center">{{ $property->value }}</div>

    </div>
    @if ($editMode)
        <x-modal-full-screen closeEventName="property.edit.close">
            <x-slot name="title">@lang('Edit'): {{ $property->attribute->label }}</x-slot>
            <x-slot name="body">
                <div class="items-center p-2 m-2">
                        <input type="text" wire:model='property.value' class="w-full border border-gray-300 roudned-md">
                </div>

            </x-slot>
            <x-slot name="buttons">
                <button class="bg-gradient-base border border-blue-600 hover:bg-white text-white hover:text-blue-600 font-bold py-2 px-4 rounded" wire:click='save'>
                    @lang('Save')
                </button>
                <button class="bg-red-600 border border-red-600 hover:bg-red-800 hover:border-red-800 text-white font-bold py-2 px-4 rounded" wire:click='deleteVehicelProperty'>
                    @lang('Delete')
                </button>
                <button class="bg-gray-400 hover:bg-gray-700 font-semibold hover:text-white py-2 px-4 rounded" wire:click='closeEdit'>
                    @lang('Close')
                </button>
            </x-slot>
        </x-modal-full-screen>
    @endif
</div>
