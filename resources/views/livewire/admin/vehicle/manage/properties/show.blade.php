<div class="my-2 rounded-md shadow-xl">
    <div class="grid grid-cols-1 p-4 text-xl font-semibold text-gray-800 border border-gray-100 rounded-md shadow-xl cursor-pointer"
        wire:click='toggleShow'>
        <h3>{{ __('Mange properties') }}</h3>
    </div>

    @if ($show)
        <div class="flex justify-end m-2 mb-6">
            <button wire:click='showAddProperty'
                class="border font-bold uppercase text-xs mt-5 py-2 px-4 rounded cursor-pointer
                @if ($vehicle->id)
                    bg-gradient-base border-blue-600 hover:bg-white text-white hover:text-blue-600
                @else
                    bg-gray-600 border-gray-600 hover:bg-gray-500 text-white
                @endif">
                {{ __('Add property') }}
            </button>
            @if ($addMode)
                <x-modal-full-screen closeEventName="properties.add.hide">
                    <x-slot name="title">@lang('Add vehicle property')</x-slot>
                    <x-slot name="body">
                            @if ($errors->any())
                                <div class="p-6 bg-red-300 border border-red-400 rounded-lg">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ __($error) }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                        <div class="grid items-start grid-cols-2 gap-4">

                            <livewire:component.dropdown.attriputes selectAction="prpperties.setPropertyDataForNewVehicleProperty"/>
                            <div class="flex items-center rounded-md bg-gray-50">
                                <input wire:model='attriputeValue' type="text" placeholder="{{ __('Enter value') }}"
                                    class="w-full h-10 px-2 py-2 leading-tight text-gray-700 border-0 rounded-md shadow-md bg-gray-50 focus:outline-none focus:ring-transparent">
                            </div>
                        </div>
                    </x-slot>
                    <x-slot name="buttons">
                        <button
                            class="px-4 py-2 font-bold text-white border border-blue-600 rounded bg-gradient-base hover:bg-white hover:text-blue-600"
                            wire:click='saveNewVehicleProperty'>
                            @lang('Save')
                        </button>
                        <button class="px-4 py-2 font-semibold bg-gray-400 rounded hover:bg-gray-700 hover:text-white"
                            wire:click='hideAddProperty'>
                            @lang('Close')
                        </button>
                    </x-slot>
                </x-modal-full-screen>
            @endif
        </div>
        <div class="grid grid-cols-1 gap-6 p-4 lg:grid-cols-3">
            @foreach ($properties as $property)
                <livewire:admin.vehicle.manage.properties.item :property="$property"
                    :key="'property_' . $property->id . time()" />
            @endforeach
        </div>
    @endif
</div>
