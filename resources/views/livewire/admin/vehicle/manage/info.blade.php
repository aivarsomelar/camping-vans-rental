<div>
    <h2 class="font-semibold text-xl text-gray-800 leading-tight items-center">
        {{ ($isNew) ? __('Create Vehicle') : __('Vehicle managment') }}: {{ $vehicle->name }} {{ $vehicle->model }}
        <svg wire:click="showEditMode" xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 inline-block mx-2 cursor-pointer" fill="none" viewBox="0 0 24 24" stroke="currentColor">
            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M11 5H6a2 2 0 00-2 2v11a2 2 0 002 2h11a2 2 0 002-2v-5m-1.414-9.414a2 2 0 112.828 2.828L11.828 15H9v-2.828l8.586-8.586z" />
          </svg>
    </h2>
    @if ($editMode)
        <x-modal-full-screen closeEventName="info.edit.hide">
            <x-slot name="title">{{ __('Edit van info') }}</x-slot>
            <x-slot name="body">
                @if ($errors->any())
                    <div class="border border-red-400 bg-red-300 rounded-lg p-6">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ __($error) }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="grid grid-cols-2 gap-4 items-start my-2">
                    <div class="flex items-center bg-gray-50 rounded-md">
                        <input type="text" placeholder="{{ __('Name') }}" wire:model='vehicle.name'
                            class="w-full rounded-md bg-gray-50 text-gray-700 leading-tight focus:outline-none py-2 px-2 border-0 focus:ring-transparent h-10 shadow-md">
                    </div>
                    <div class="flex items-center bg-gray-50 rounded-md">
                        <input type="text" placeholder="{{ __('Model') }}" wire:model='vehicle.model'
                            class="w-full rounded-md bg-gray-50 text-gray-700 leading-tight focus:outline-none py-2 px-2 border-0 focus:ring-transparent h-10 shadow-md">
                    </div>
                </div>
            </x-slot>
            <x-slot name="buttons">
                <button class="bg-gradient-base border border-blue-600 hover:bg-white text-white hover:text-blue-600 font-bold py-2 px-4 rounded" wire:click='save'>
                    @if (!$vehicle->id)
                        @lang('save')
                    @else
                        @lang('update')
                    @endif
                </button>
                <button class="bg-gray-400 hover:bg-gray-700 font-semibold hover:text-white py-2 px-4 rounded" wire:click='hideEditMode'>
                    @lang('cancel')
                </button>
            </x-slot>
        </x-modal-full-screen>
    @endif
</div>
