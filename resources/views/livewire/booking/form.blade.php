<div id="book_me" class="w-full p-8 bg-white rounded shadow sm:p-12">
    <p class="text-3xl font-bold leading-7 text-center">@lang('Book me')</p>
    @if ($error)
        <div class="items-center w-full my-2">
            <x-alert.error message="Sending message failed" sub-message="Please refresh page and try again"></x-alert.error>
        </div>
    @elseif($success)
        <div class="items-center w-full my-2">
            <x-alert.success message="Message sent to customer service!" sub-message="We contact you as soon as possible"></x-alert.success>
        </div>
    @endif
    <form action="" method="post">
        <div class="mt-12 md:flex">
            <div class="flex flex-col w-full md:w-1/2">
                <label class="font-semibold leading-none">@lang('Full name')</label>
                <input type="text" wire:model="name"
                       class="p-3 mt-4 leading-none text-gray-900 bg-gray-100 border border-gray-200 rounded focus:outline-none focus:border-blue-700"/>
                @error('name') <span class="text-red-600">{{ __($message) }}</span> @enderror
            </div>
            <div class="flex flex-col w-full mt-4 md:w-1/2 md:ml-6 md:mt-0">
                <label class="font-semibold leading-none">@lang('Phone')</label>
                <input type="text" wire:model="phone"
                       class="p-3 mt-4 leading-none text-gray-900 bg-gray-100 border border-gray-200 rounded focus:outline-none focus:border-blue-700"/>
                @error('phone') <span class="text-red-600">{{ __($message) }}</span> @enderror
            </div>
        </div>
        <div class="items-center mt-8 md:flex">
            <div class="flex flex-col w-full">
                <label class="font-semibold leading-none">@lang('Email')</label>
                <input type="email" wire:model="email"
                       class="p-3 mt-4 leading-none text-gray-900 bg-gray-100 border border-gray-200 rounded focus:outline-none focus:border-blue-700"/>
                @error('email') <span class="text-red-600">{{ __($message) }}</span> @enderror
            </div>
        </div>
        <div class="mt-12 md:flex">
            <div class="flex flex-col w-full md:w-1/2">
                <label class="font-semibold leading-none">@lang('Rent start date')</label>
                <x-input.date wire:model="startDate" :error="$errors->first('startDate')" placeholder="Rent start date" dissabledDates="bookedDates" />
                @error('startDate') <span class="text-red-600">{{ __($message) }}</span> @enderror
            </div>
            <div class="flex flex-col w-full mt-4 md:w-1/2 md:ml-6 md:mt-0">
                <label class="font-semibold leading-none">@lang('Rent end date')</label>
                <x-input.date wire:model="endDate" :error="$errors->first('endDate')" placeholder="Rent end date" dissabledDates="bookedDates" />
                @error('endDate') <span class="text-red-600">{{ __($message) }}</span> @enderror
            </div>
        </div>
        <div>
            <div class="flex flex-col w-full mt-8">
                <label class="font-semibold leading-none">@lang('Message')</label>
                <textarea type="text" wire:model="message"
                          class="h-40 p-3 mt-4 text-base leading-none text-gray-900 bg-gray-100 border border-gray-200 rounded focus:oultine-none focus:border-blue-700"
                ></textarea>
                @error('message') <span class="text-red-600">{{ __($message) }}</span> @enderror
            </div>
        </div>
        <div class="relative flex items-start mt-8">
            <div class="flex items-center h-5">
              <input id="rentalConditions" name="rentalConditions" type="checkbox"
                class="focus:ring-blue-700 h-4 w-4 text-blue-700 border-gray-300 rounded  @error('haveReadRentalConditions') border-red-600 @enderror"
                wire:model="haveReadRentalConditions">
            </div>
            <div class="ml-3">
              <label for="rentalConditions" class="font-medium @error('haveReadRentalConditions') text-red-600 @enderror">@lang('I have read and understand rental contisions')</label>
            </div>
          </div>
        <div class="flex items-center justify-center w-full">
            <button wire:click.prevent="sendMessage" wire:loading.remove
                class="px-10 py-4 font-semibold leading-none text-white bg-blue-700 rounded mt-9 hover:bg-blue-600 focus:ring-2 focus:ring-offset-2 focus:ring-blue-700 focus:outline-none">
                @lang('Send message')
            </button>
            <button wire:loading wire:target="sendMessage" disabled
                    class="px-10 py-4 font-semibold leading-none text-white bg-blue-500 rounded mt-9 focus:outline-none">
                @lang('Please wait')
            </button>
        </div>
    </form>
</div>
