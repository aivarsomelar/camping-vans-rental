<div class="bg-white shadow-md rounded-lg mb-4">
    <div class="flex items-center bg-gray-50 rounded-md">
        <div class="px-2">
            <svg class="fill-current text-gray-500 w-6 h-6" xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 24 24">
                <path class="heroicon-ui"
                    d="M16.32 14.9l5.39 5.4a1 1 0 0 1-1.42 1.4l-5.38-5.38a8 8 0 1 1 1.41-1.41zM10 16a6 6 0 1 0 0-12 6 6 0 0 0 0 12z" />
            </svg>
        </div>
        <input
            class="w-full rounded-md bg-gray-50 text-gray-700 leading-tight focus:outline-none py-2 px-2 border-0 focus:ring-transparent h-10"
            id="search" type="text" placeholder="{{ __('Search') }}" wire:model="keyword" wire:change="triggerSearch" wire:keydown.enter="triggerSearch">
    </div>
    @if ($searchResults)
        @foreach ($searchResults as $result)
            <div class="py-3 text-sm">
                <div class="flex justify-start cursor-pointer text-gray-700 hover:text-blue-400 hover:bg-blue-100 rounded-md px-2 py-2 my-2">
                    <span class="bg-gray-400 h-2 w-2 m-2 rounded-full"></span>
                    <div class="flex-grow font-medium px-2" wire:click="select({{ $result }})">{{ $result->label }}</div>
                </div>
            </div>
        @endforeach
    @endif
</div>
