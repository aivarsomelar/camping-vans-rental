<div
    class="border rounded-lg md:rounded-r-none text-center p-5 mx-auto md:mx-0 my-2 md:my-4 bg-gray-100 font-medium z-10 shadow-lg w-full lg:w-2/3">
    <div class="">@lang('Starting from')</div>
    <div id="month" class="font-bold text-6xl">{{ $smallestPrice }}€ @lang('day')</div>
    <hr>
    @foreach ($prices as $price)
        <div class="my-3">{{ $price->days }} @lang('day') - {{ $price->price }}€ @lang('per day')</div>
        <hr>
    @endforeach
    <div class="relative">
        <div class="text-sm mt-3 font-bold">@lang('Price list is valid')</div>
        <div class="text-sm">

            <span class="font-bold text-red-600">
            {{ (new \Carbon\Carbon($prices->first()->period_start))->format('d.m.Y') }} -
            {{ (new \Carbon\Carbon($prices->first()->period_end))->format('d.m.Y') }}
            </span>
        </div>
        <span class="absolute left-0 top-0 mt-2 cursor-pointer" wire:click='setActivePeriod({{ false }})'>
            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 19l-7-7 7-7" />
            </svg>
        </span>
        <span class="absolute right-0 top-0 mt-2 cursor-pointer" wire:click='setActivePeriod({{ true }})'>
            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 5l7 7-7 7" />
            </svg>
        </span>
    </div>
    <a href="#book_me">
        <div
            class="bg-gradient-base border border-blue-600 hover:bg-white text-white hover:text-blue-600 font-bold uppercase text-xs mt-5 py-2 px-4 rounded cursor-pointer">
            @lang('Book me')
        </div>
    </a>
</div>
