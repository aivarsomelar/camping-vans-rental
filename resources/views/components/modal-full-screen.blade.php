<div class="fixed bottom-0 left-0 z-40 flex items-center justify-center w-full h-full bg-gray-800">
    <div class="w-1/2 bg-white rounded-lg">
        <div class="flex flex-col items-start p-4">
            <div class="flex items-center w-full">
            <div class="text-lg font-medium text-gray-900">{{ $title }}</div>
            <svg class="w-6 h-6 ml-auto text-gray-700 cursor-pointer fill-current" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 18 18" x-on:click="$wire.emit('{{ $closeEventName }}')">
                <path d="M14.53 4.53l-1.06-1.06L9 7.94 4.53 3.47 3.47 4.53 7.94 9l-4.47 4.47 1.06 1.06L9 10.06l4.47 4.47 1.06-1.06L10.06 9z"/>
            </svg>
            </div>
            <hr>
            <div class="w-full">{{ $body }}</div>
            <hr>
            <div class="ml-auto">
                {{ $buttons }}

            </div>
        </div>
    </div>
</div>
