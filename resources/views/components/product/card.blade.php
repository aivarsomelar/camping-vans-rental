<div class="shadow-xl my-4">
    <img class="h-64 items-center" src="{{ Storage::url($imgPath) }}">
    <div class="p-2">
        <h3 class="font-semibold leading-tight text-lg">{{ $title }}</h3>
        <div class="w-full text-right font-bold mt-2">
            <span class="text-red-600">@lang('Price'):</span>
            {{ $price }}€ @lang('day')
        </div>
    </div>
</div>
