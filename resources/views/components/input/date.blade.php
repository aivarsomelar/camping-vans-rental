@props([
    'error' => null,
    'placeholder' => 'select date',
    'dissabledDates' => null // json string
])

<div
    x-data="{ value: @entangle($attributes->wire('model')), dissabledDates: @entangle($dissabledDates) }"
    x-on:change="value = $event.target.value"
    x-init="
        new Pikaday({
            field: $refs.input,
            'format': 'DD.MM.YYYY',
            firstDay: 1,
            minDate: new Date(),
            disableDayFn: function(date) {

                let isDisabled = false
                if(dissabledDates){
                    let dissabledDatesJson = JSON.parse(dissabledDates)

                    dissabledDatesJson.forEach(function(dissabledDate) {
                        {{-- Set time same for every date, we only want to compare dates, not time --}}
                        if(date.toDateString() === new Date(dissabledDate).toDateString()) {
                            isDisabled = true
                        }
                    })
                }

                return isDisabled
            }
        });"
>
    <input
        {{ $attributes->whereDoesntStartWith('wire:model') }}
        x-ref="input"
        x-bind:value="value"
        placeholder="{{ __($placeholder) }}"
        type="text"
        class="w-full rounded-md bg-gray-50 text-gray-700 leading-tight focus:outline-none py-2 px-2 border-0 focus:ring-transparent h-10 shadow-md
        @if($error) focus:ring-danger-500 focus:border-danger-500 border-danger-500 text-danger-500 @else focus:ring-primary-500 focus:border-primary-500 @endif"
    />
</div>
