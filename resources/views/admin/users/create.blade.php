<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Invite new user') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="overflow-hidden grid grid-cols-3">
                <div>
                    <h3 class="font-semibold text-lg text-gray-800 leading-tight">
                        {{ __('Invite new user') }}
                    </h3>
                    <p class="my-2 text-md text-gray-600">{{ __('Send email to worker with registration linke. Link expires after 24h') }}</p>
                </div>
                <div class="col-span-2">
                    <livewire:admin.users.create.invite></livewire:admin.users.create.invite>
                </div>

            </div>

            <livewire:admin.users.create.invitations></livewire:admin.users.create.invitations>
        </div>
    </div>
</x-app-layout>
