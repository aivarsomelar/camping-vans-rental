<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="mb-6 shadow-xl rounded-md">
                <div class="my-2">
                    <div class="grid grid-cols-1 text-xl font-semibold text-gray-800 p-4 border border-gray-100 cursor-pointer">
                        <h3>{{ __('New Bookings') }}</h3>
                    </div>
                    <div class="grid grid-cols-1 lg:grid-row-1 gap-2 lg:gap-1 p-4">
                        @foreach ($newBookings as $booking)
                            <livewire:admin.vehicle.manage.booking.item :booking="$booking" :vehicle="$booking->van" :key="'booking_' .$booking->id . time()"  :forceReload="true"/>
                        @endforeach
                    </div>
                </div>
                <div class="border-b border-gray-200 w-full"></div>
                <div class="my-2">
                    <div class="grid grid-cols-1 text-xl font-semibold text-gray-800 p-4 border border-gray-100 cursor-pointer">
                        <h3>{{ __('Unconfirmed Bookings') }}</h3>
                    </div>
                    <div class="grid grid-cols-1 lg:grid-row-1 gap-2 lg:gap-1 p-4">
                        @foreach ($waiting as $booking)
                            <livewire:admin.vehicle.manage.booking.item :booking="$booking" :vehicle="$booking->van" :key="'booking_' .$booking->id . time()" :forceReload="true"/>
                        @endforeach
                    </div>
                </div>
                <div class="border-b border-gray-200 w-full"></div>
                <div class="my-2">
                    <div class="grid grid-cols-1 text-xl font-semibold text-gray-800 p-4 border border-gray-100 cursor-pointer">
                        <h3>{{ __('Confirmed Bookings') }}</h3>
                    </div>
                    <div class="grid grid-cols-1 lg:grid-row-1 gap-2 lg:gap-1 p-4">
                        @foreach ($confirmed as $booking)
                            <livewire:admin.vehicle.manage.booking.item :booking="$booking" :vehicle="$booking->van" :key="'booking_' .$booking->id . time()" :forceReload="true"/>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
