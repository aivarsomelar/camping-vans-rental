<x-app-layout>
    <x-slot name="header">
        <livewire:admin.vehicle.manage.info :vehicle="$vehicle" :isNew="true" />
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="mb-6">
                <livewire:admin.vehicle.manage.prices.show :vehicle="$vehicle" />
            </div>
            <div class="mb-6">
                <livewire:admin.vehicle.manage.image.gallery :vehicle="$vehicle" />
            </div>
            <div class="mb-6">
                <livewire:admin.vehicle.manage.properties.show :vehicle="$vehicle" />
            </div>
        </div>
    </div>
</x-app-layout>
