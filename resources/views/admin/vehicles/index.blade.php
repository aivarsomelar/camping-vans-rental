<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Vehicle managment') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="overflow-hidden">
                @can('create vehicles')
                    <div class="w-full text-right p-2">
                            <a href="{{ route('vehicleManagment.create') }}"><button
                                class="bg-gradient-base border border-blue-600 hover:bg-white text-white hover:text-blue-600 font-bold uppercase text-xs mt-5 py-2 px-4 rounded cursor-pointer">
                                {{ __('Add new vehicle') }}
                            </button>
                        </a>
                    </div>
                @endcan
                @can('manage vehicles')
                <div class="bg-white shadow-md rounded my-6">
                    <table class="min-w-max w-full table-auto">
                        <thead>
                            <tr class="bg-gray-200 text-gray-600 uppercase text-sm leading-normal">
                                <th class="py-3 px-6 text-left">Name</th>
                                <th class="py-3 px-6 text-left">Model</th>
                                <th class="py-3 px-6 text-center">Actions</th>
                            </tr>
                        </thead>
                        <tbody class="text-gray-600 text-sm font-light">
                            @foreach ($vehicles as $vehicle)
                                <tr class="border-b border-gray-200 hover:bg-gray-100">
                                    <td class="py-3 px-6 text-left whitespace-nowrap">
                                        <a href="{{ route('vehicleManagment.show', ['vehicleManagment' => $vehicle]) }}" class="cursor-pointer">
                                        <div class="flex items-center">
                                            <span class="font-medium">{{ $vehicle->name }}</span>
                                        </div>
                                        </a>
                                    </td>
                                    <td class="py-3 px-6 text-left">
                                        <a href="{{ route('vehicleManagment.show', ['vehicleManagment' => $vehicle]) }}" class="cursor-pointer">
                                        <div class="flex items-center">
                                            <span>{{ $vehicle->model }}</span>
                                        </div>
                                        </a>
                                    </td>
                                    <td class="py-3 px-6 text-center">
                                        <div class="flex item-center justify-center">
                                            @can('delete vehicles')
                                                <form action="{{ route('vehicleManagment.destroy', [ 'vehicleManagment' => $vehicle ]) }}" method="POST">
                                                    @csrf
                                                    <input type="hidden" name="_method" value="DELETE">
                                                    <button class="w-4 mr-2 transform hover:text-purple-500 hover:scale-110">
                                                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                                            stroke="currentColor">
                                                            <path stroke-linecap="round" stroke-linejoin="round"
                                                                stroke-width="2"
                                                                d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16" />
                                                        </svg>
                                                    </button>
                                                </form>
                                            @endcan
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                @endcan
            </div>
        </div>
    </div>
</x-app-layout>
