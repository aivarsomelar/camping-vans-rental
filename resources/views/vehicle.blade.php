<x-guest-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ $vehicle->name }} {{ $vehicle->model }}
        </h2>
    </x-slot>

    <x-carousel
        :images="$vehicle->images()->pluck('path')->toArray()"></x-carousel>
    <div class="my-2 lg:grid lg:grid-cols-2">
        <div class="grid grid-cols-2 my-2 md:my-4 py-2">
            @foreach($vehicle->attributes as $vanAttribute)
                <div class="grid grid-cols-2">
                    <span>{{ __($vanAttribute->attribute->label) }}: </span><span>{{ __($vanAttribute->value) }}</span>
                </div>
            @endforeach
        </div>
        <div class="flex flex-col md:flex-row md:transform md:scale-75 lg:scale-100 justify-center">
            <livewire:components.price-list :vehicle="$vehicle">
        </div>
    </div>

    <livewire:booking.form :vehicle="$vehicle"></livewire:booking.form>
</x-guest-layout>
