<div>
        <h2>{{ $data['name'] }} soovib rentida bussi</h2>
    <ul>
        <li>Nimi: {{ $data['name'] }}</li>
        <li>Buss: {{ $data['vehicle'] }}</li>
        <li>Telefon: {{ $data['phone'] }}</li>
        <li>Email: {{ $data['email'] }}</li>
        <li>Rendi algus: {{ $data['startDate'] }}</li>
        <li>Rendi lõpp: {{ $data['endDate'] }}</li>
    </ul>
    Sõnum: <p>{{ $data['message'] }}</p>
</div>
