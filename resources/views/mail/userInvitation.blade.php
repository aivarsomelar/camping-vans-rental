<div>
    <h2>{{ __('You have been invited to join ') . env('APP_NAME') }}</h2>
    <p>{{ __('Click link to finish registration') }}</p>
    <a href="{{ env('APP_URL') . '/register/' . $token }}">{{ env('APP_URL') . '/register/' . $token }}</a>
</div>
