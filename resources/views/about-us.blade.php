<x-guest-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            @lang('Camping cottage rental conditions')
        </h2>
    </x-slot>
    <div id="about-us-content">
        <h3 class="font-bold text-lg text-gray-800 leading-tight mt-4">
            Üldtitingimused
        </h3>
        <p class="my-2">
            Matkaauto rentimiseks esitab rentnik kehtiva juhiloa ning isikut tõendava dokumendi (ID kaart või pass). Matkabussi rentimiseks peab rentnik olema vähemalt 21-aastane ning omama vähemalt 3-aastast B-kategooria juhistaaži. A-klassi täisintegreeritud mudelite (LUX-klass) puhul on nõutav juhistaaž vähemalt 5 aastat. Kui soovite sõidukit rentida ettevõtte nimele, siis peab lepingu allkirjastama juhatuse liige või selleks volitatud isik, esitades rendifirmale vastavasisulise ning sobiva volikirja koos volituse kehtivust tõendavate dokumentidega (volikiri, garantiikiri, äriregistri B-kaardi koopia). Samuti kuulub sellisel juhul lisaks rendilepingule allkirjastamisele käendusleping, millega sõiduki kasutaja tagab ettevõtte võetud kohustusi.
        </p>
        <p class="my-2">
            Rentnikule antakse üle tangitud, puhastatud ning tehniliselt korras matkaauto. Sõidukit vastu võttes soovitame Teil see hoolikalt üle vaadata ning fikseerida sõidukit vastu võttes avastatud vigastused ning puudujäägid (sh kriimustused, puuduv või katkine varustus jms), vastasel juhul võite jääda vastutavaks vigastuste/puuduste eest, mis olid sõidukil juba enne Teie kasutusse andmist. Rendileandja ei vastuta otseselt ega kaudselt kahjude eest, mis võivad Rentnikule tuleneda matkaauto tehnilisest rikkest ning kui seoses eelnevaga reis katkeb või viibib.
        </p>
        <p class="my-2">
            Matkaauto antakse rentnikule üldjuhul üle täidetud kütusepaagiga. Matkaauto tagastamisel mitte täis paagiga arvestatakse renditasule juurde tasu puuduoleva kütuse iga liitri eest vastavalt Circle K kehtivale hinnakirjale ning tasu tankimise eest 50 EUR (sisaldab käibemaksu). Rendileandjal on õigus antud summad kinni pidada tagatisrahast. Valesti tangitud kütusest tekitatud kahju korvab rentnik.
        </p>

        <h3 class="font-bold text-lg text-gray-800 leading-tight mt-4">
            Broneerimine, tagatisraha ning tasumine
        </h3>
        <p class="my-2">
            Matkaauto broneerimistasu on 30% rendiperioodi kogumaksumusest, minimaalselt 300 EUR. Broneerimise kinnitamiseks loetakse broneerimistasu laekumist eletri OÜ arveldusarvele ning see tuleb tasuda 3 päeva jooksul alates rendileandjalt kirjaliku kinnituse või vastavasisulise arve saamist broneeringu kinnituse kohta. Kõrghooajal broneerimisel (perioodil 01.06. kuni 31.08.), tehes broneeringu vähem kui 30 päeva enne rendiperioodi algust tasub rentnik broneerimisel broneerimistasu 50% rendihinnast.
        </p>
        <p class="my-2">
            Rentnikule kinnitatud broneeringutele võib teha muudatusi alates reserveeringu tegemise kuupäevast kuni vähemalt 90 päeva enne kokkulepitud rendiperioodi algust ning seda vaid juhul, kui rendileandjal on võimalik pakkuda alternatiivseid võimalusi ja alternatiivne broneering vastab kogusummalt esialgsele. Broneerimistasu ei tagastata, vaid see arvestatakse renditasu sisse. Broneeringu kliendipoolsel tühistamisel vastutab klient broneerimistasu ulatuses ning broneerimistasu ei tagastata.
        </p>
        <p class="my-2">
            Matkaauto rentnik maksab lepingu nõuete täitmise tagatisraha 500 EUR (LUX klassi täisintegreeritud mudelite puhul kuni 1000 EUR), mis lepingu lõpetamisel ning nõuete puudumise korral korral rentnikule tagastatakse.
        </p>
        <p class="my-2">
            Ülejäänud renditasu ning tagatisraha tasutakse enne rendiperioodi algust vastavalt rendilepingus kokkulepitud tähtaegadel. Lõplik arveldamine toimub pärast sõiduki tagastamist rendileandjale. Renditasu on võimalik tasuda nii pangaülekandega kui sularahas.
        </p>
        <p class="my-2">
            Rendihind sisaldab õlide, hoolduse, kohustusliku liikluskindlustuse ja täiskasko kindlustuse maksumust. Rendihind ei sisalda autokütuse maksumust.
        </p>

        <h3 class="font-bold text-lg text-gray-800 leading-tight mt-4">
            Kindlustused, vastutus kolmandate isikute ees
        </h3>
        <p class="my-2">
            Matkabusside rendi hinnad sisaldavad kaskokindlustust (avarii, vandalism) rentniku omavastutusega 500 EUR (LUX klassi täisintegreeritud mudeli puhul 1000 EUR) ja varguskindlustust rentniku omavastutusega 15% sõiduki maksumusest (sõiduki väärtus toodud rendilepingus). Sõidukite rehvid ja veljed ei ole kindlustatud ning nii rehvide kui ka velgede osas kannab rentnik täielikku materiaalset vastutust. Matkabussi ärandamise või varguse korral on rentnik kohustatud esitama auto võtmed ja sõiduki registreerimistunnistuse. Ärandamine või vargus peab olema rentniku poolt registreeritud varguse või ärandamise toimumiskoha järgses politseijaoskonnas. Liiklusõnnetuse korral on rentnik kohustatud teatama sellest kindlasti politseisse ja võtma kirjaliku tõendi õnnetuse toimumise kohta koos asjaolude selgitamisega kindlustuse jaoks. Kohe tuleb ühendust võtta ka rendifirmaga (kontaktandmed leiate rendilepingult).
        </p>
        <p class="my-2">
            Kõik rentniku ja/või matkasuvila kasutajate poolt rendiperioodi jooksul põhjustatud nõuded kolmandatelt isikutelt (näiteks liikluseeskirjade rikkumised, parkimistrahvid, viivistasu otsused, auto teisaldamised jms) kuuluvad rentniku ja/või auto kasutajate poolt täielikule hüvitamisele ka tagantjärgi.
        </p>

        <h3 class="font-bold text-lg text-gray-800 leading-tight mt-4">
            Sõiduki üleandmine ja tagastamine, rendiperiood
        </h3>
        <p class="my-2">
            Rendiperiood algab esimesel rendipäeval alates kell 14:00 ning lõppeb rendiperioodi lõpp-päeval hiljemalt kell 11:00, kui poolte vahel ei ole kokkulepitud teisiti. Kui matkaauto tagastatakse rohkem kui 1 tund hiljem eelnevalt kokkulepitud ajast, on Rendileandjal õigus rendisummale lisada täiendavalt ühe rendipäeva tasu. Kui Rentnik hilineb sõiduki tagastamisega rohkem kui ühe päeva, siis on Rendileandjal õigus nõuda hilinetud päevade eest topelt renti.
        </p>
        <p class="my-2">
            Matkaauto tagastatakse Rentniku poolt Rendileandjale siseruumid hoolikalt puhastatuna, reoveepaak ning WC-kassett tühjendatuna sarnaselt sellele, milline auto oli rendile võtmise hetkel. Juhul, kui sõiduk tagastatakse koristamata ning see ei ole eelnevalt Rentniku ja Rendileandja vahel kokku lepitud, on Rendileandjal õigus nõuda leppetrahvi. Ettetellimisel on matkaauto siseruumide koristuse hind 80 EUR. Reoveepaakide ning WC-kasseti tühjendamise ja puhastamise hind on 30 EUR. Autos ei tohi ilma eelneva kokkuleppeta vedada lemmikloomi. Matkaautos suitsetamine on rangelt keelatud. Nõude rikkumise korral on Rendileandjal õigus nõuda leppetrahvi 300 EUR (LUX klassi täisintegreeritud mudeli puhul 500 EUR). Ülalnimetatud summad on Rendileandjal õigus kinni pidada tagatisrahast.
        </p>
        <p class="my-2">
            Autoelamu tagastamisel kuulub välipesu sõiduki rendihinna sisse ning selle teostab Rendileandja omal kulul.
        </p>
        <p class="my-2">
            Sõiduki tagastamisel teostavad auto ülevaatuse Rendileandja ja Rentnik koos. Vead, mida ei olnud rendile andmise hetkel ja mida ei saa lugeda normaalseks kulumiseks korvab Rentnik autoesinduse hinnakirja alusel.
        </p>
        <p class="my-2">
            Autoelamu rendi minimaalne periood on kolm ööpäeva. Kõrghooajal (01.06. kuni 31.08. ja koolivaheaegadel) on minimaalne rendiperiood 7 päeva. Matkaautol läbisõidupiirangut ei ole.
        </p>
        <p class="my-2">
            Sõiduki rentnikule üleandmisel või rentnikupoolsel tagastamisel pühapäeviti või riiklikel pühadel lisandub renditasule 30 EUR.
        </p>

        <h3 class="font-bold text-lg text-gray-800 leading-tight mt-4">
            Varustus
        </h3>
        <p class="my-2">
            Talveperioodil on matkaautod varustatud talverehvidega. Kõik meie autoelamud on varustatud GPS-seadmetega, mis võimaldavad Rendileandjal jälgida sõiduki sihtotstarbelist kasutamist. Vastavalt poolte kokkuleppele annab Rendileandja Rentniku kasutusse lisavarustuse, millise täpne nimekiri on fikseeritud rendilepingus.
        </p>

        <h3 class="font-bold text-lg text-gray-800 leading-tight mt-4">
            Lepingu kehtivus
        </h3>
        <p class="my-2">
            Iga allakirjutatud leping on siduv – ka juhul, kui Rentnik ei ole seda läbi lugenud või sellest aru saanud. Soovitame alati enne lepingu allkirjastamist tingimused täpselt läbi lugeda ning vajadusel täpsustada. Hilisemaid pretensioone ei rahuldata või lahendatakse kohtus.
        </p>

        <h3 class="font-bold text-lg text-gray-800 leading-tight mt-4">
            Sõitmine Eestist väljapoole
        </h3>
        <p class="my-2">
            Klient ei tohi renditud matkaautoga sõita väljapoole Euroopa Liitu (v.a. Norra, Šveits, Lichtenstein, Andorra). Vastav soov kooskõlastada kirjalikult rendifirmaga enne rendiperioodi algust vähemalt 14 päeva.
        </p>

        <h3 class="font-black text-lg text-gray-800 leading-tight mt-4">
            Täpsemad tingimused on toodud Rendileandja ning Rentniku vahel sõlmitavas rendilepingus.
        </h3>
    </div>
</x-guest-layout>
