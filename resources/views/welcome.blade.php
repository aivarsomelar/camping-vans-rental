<x-guest-layout>
    @inject('carbon', 'Carbon\Carbon')
    <x-slot name="header">
        <h2 class="text-xl font-semibold leading-tight text-gray-800">
            @lang('Vehicles')
        </h2>
    </x-slot>
    <div class="relative flex">
        <img class="flex w-full" src="{{ asset('assets/RMB_banner.jpg') }}" />
        {{-- <div class="absolute hidden w-1/2 p-4 font-bold text-red-600 text-8xl top-56 left-24 -rotate-6 xl:block">MAIKUU SOODUSPAKKUMINE</div> --}}
    </div>
    {{-- <div class="py-6 ">
        <span class="text-2xl text-red-600">Maikuus rendihinnad :</span>
        <ul class="list-disc list-inside">
            <li>-10% - LÜHIRENT</li>
            <li>-15% - RENT ALATES 7 PÄEVA</li>
            <li>-20% - RENT ALATES 10 PÄEVA</li>
        </ul>
        <span class="text-sm">
            Hinnad kehtivad rentidele kuni 31.05.2023
        </span>
    </div> --}}
    <div class="lg:grid lg:grid-cols-3 lg:gap-4">
        @foreach($vehicles as $vehicle)
            @if ($vehicle->images()->first() and $vehicle->prices->where('period_start', '<=', $now)->where('period_end', '>=', $now))
                <a href="{{ route('vehicle', ['van' => $vehicle]) }}">
                    <x-product.card imgPath="{{ $vehicle->images()->first()->path }}" title="{{ $vehicle->name }} {{ $vehicle->model }}"
                                    price="{{ $vehicle->prices->where('period_start', '<=', $now)->where('period_end', '>=', $now)->min('price') }}"></x-product.card>
                </a>
            @endif
        @endforeach
    </div>
</x-guest-layout>

