import './bootstrap';

import Alpine from 'alpinejs';
import '../../node_modules/pikaday/css/pikaday.css';
import Pikaday from 'pikaday';
window.Pikaday = Pikaday;
window.Alpine = Alpine;

Alpine.start();
