<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RolesAndPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $adminRole = Role::create(['name' => 'admin']);
        $worker = Role::create(['name' => 'worker']);

        $addUsersPermission = Permission::create(['name' => 'add users']);
        $editUsersPermission = Permission::create(['name' => 'edit users']);
        $deleteUsersPermission = Permission::create(['name' => 'delete users']);

        $adminRole->givePermissionTo($addUsersPermission);
        $adminRole->givePermissionTo($editUsersPermission);
        $adminRole->givePermissionTo($deleteUsersPermission);

        $vehicleManagmentPermission = Permission::create(['name' => 'manage vehicles']);
        $vehicleCreationPermission = Permission::create(['name' => 'create vehicles']);
        $vehicleEditPermission = Permission::create(['name' => 'edit vehicles']);
        $vehicleDeletePermission = Permission::create(['name' => 'delete vehicles']);

        $adminRole->givePermissionTo($vehicleManagmentPermission);
        $adminRole->givePermissionTo($vehicleCreationPermission);
        $adminRole->givePermissionTo($vehicleEditPermission);
        $adminRole->givePermissionTo($vehicleDeletePermission);
        $worker->givePermissionTo($vehicleManagmentPermission);
        $worker->givePermissionTo($vehicleCreationPermission);
        $worker->givePermissionTo($vehicleEditPermission);
    }
}
