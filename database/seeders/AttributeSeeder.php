<?php

namespace Database\Seeders;

use App\Models\Attribute;
use Illuminate\Database\Seeder;

class AttributeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Attribute::create(['id' => 1, 'label' => 'length', 'order' => 0]);
        Attribute::create(['id' => 2, 'label' => 'height', 'order' => 0]);
        Attribute::create(['id' => 3, 'label' => 'year', 'order' => 0]);
        Attribute::create(['id' => 4, 'label' => 'engine power', 'order' => 0]);
        Attribute::create(['id' => 5, 'label' => 'transmission', 'order' => 0]);
        Attribute::create(['id' => 6, 'label' => 'awning', 'order' => 0]);
        Attribute::create(['id' => 7, 'label' => 'reversing camera', 'order' => 0]);
        Attribute::create(['id' => 8, 'label' => 'sleeping places', 'order' => 0]);
        Attribute::create(['id' => 9, 'label' => 'seats', 'order' => 0]);
        Attribute::create(['id' => 10, 'label' => 'cruise control', 'order' => 0]);
        Attribute::create(['id' => 11, 'label' => 'tv', 'order' => 0]);
        Attribute::create(['id' => 12, 'label' => 'stereo', 'order' => 0]);
        Attribute::create(['id' => 13, 'label' => 'air conditioning', 'order' => 0]);
        Attribute::create(['id' => 14, 'label' => 'shower', 'order' => 0]);
        Attribute::create(['id' => 15, 'label' => 'wc', 'order' => 0]);
        Attribute::create(['id' => 16, 'label' => 'sink', 'order' => 0]);
        Attribute::create(['id' => 17, 'label' => 'lighting', 'order' => 0]);
        Attribute::create(['id' => 18, 'label' => 'garage dimensions', 'order' => 0]);
        Attribute::create(['id' => 19, 'label' => 'gps', 'order' => 0]);
    }
}
