<?php

namespace Database\Seeders;

use App\Models\Van;
use Illuminate\Database\Seeder;

class VansSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Van::create([
            'id' => 1,
            'name' => 'Mobilvetta',
            'model' => 'Kea-i90',
            'description' => '',
        ]);
        Van::create([
            'id' => 2,
            'name' => 'Mobilvetta',
            'model' => 'K-Yacht TEKNOLINE 86',
            'description' => '',
        ]);
        Van::create([
            'id' => 3,
            'name' => 'McLOUIS',
            'model' => 'MC4 873',
            'description' => '',
        ]);
        Van::create([
            'id' => 4,
            'name' => 'Fiat',
            'model' => 'Bürstner Campeo C640/ Facelift ',
            'description' => '',
        ]);
    }
}
