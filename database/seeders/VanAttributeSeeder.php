<?php

namespace Database\Seeders;

use App\Models\VanAttribute;
use Illuminate\Database\Seeder;

class VanAttributeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        VanAttribute::create(['van_id' => 1, 'attribute_id' => 1, 'value' => '7.50 m']);
        VanAttribute::create(['van_id' => 1, 'attribute_id' => 2, 'value' => '2.95 m']);
        VanAttribute::create(['van_id' => 1, 'attribute_id' => 3, 'value' => '2022']);
        VanAttribute::create(['van_id' => 1, 'attribute_id' => 4, 'value' => '140 hp']);
        VanAttribute::create(['van_id' => 1, 'attribute_id' => 5, 'value' => 'manual']);
        VanAttribute::create(['van_id' => 1, 'attribute_id' => 6, 'value' => 'yes']);
        VanAttribute::create(['van_id' => 1, 'attribute_id' => 7, 'value' => 'yes']);
        VanAttribute::create(['van_id' => 1, 'attribute_id' => 8, 'value' => 4]);
        VanAttribute::create(['van_id' => 1, 'attribute_id' => 9, 'value' => 4]);
        VanAttribute::create(['van_id' => 1, 'attribute_id' => 10, 'value' => 'yes']);
        VanAttribute::create(['van_id' => 1, 'attribute_id' => 11, 'value' => 'LCD (1 x HDMI, 1 x USB)']);
        VanAttribute::create(['van_id' => 1, 'attribute_id' => 12, 'value' => 'yes']);
        VanAttribute::create(['van_id' => 1, 'attribute_id' => 13, 'value' => 'at car part']);
        VanAttribute::create(['van_id' => 1, 'attribute_id' => 14, 'value' => 'yes']);
        VanAttribute::create(['van_id' => 1, 'attribute_id' => 15, 'value' => 'yes']);
        VanAttribute::create(['van_id' => 1, 'attribute_id' => 16, 'value' => 2]);
        VanAttribute::create(['van_id' => 1, 'attribute_id' => 17, 'value' => 'LED']);
        VanAttribute::create(['van_id' => 1, 'attribute_id' => 18, 'value' => '120cm x 90cm']);

        VanAttribute::create(['van_id' => 2, 'attribute_id' => 1, 'value' => '7.51 m']);
        VanAttribute::create(['van_id' => 2, 'attribute_id' => 2, 'value' => '2.95 m']);
        VanAttribute::create(['van_id' => 2, 'attribute_id' => 3, 'value' => '2022']);
        VanAttribute::create(['van_id' => 2, 'attribute_id' => 4, 'value' => '140 hp']);
        VanAttribute::create(['van_id' => 2, 'attribute_id' => 5, 'value' => 'manual']);
        VanAttribute::create(['van_id' => 2, 'attribute_id' => 6, 'value' => 'yes']);
        VanAttribute::create(['van_id' => 2, 'attribute_id' => 7, 'value' => 'yes']);
        VanAttribute::create(['van_id' => 2, 'attribute_id' => 19, 'value' => 'yes']);
        VanAttribute::create(['van_id' => 2, 'attribute_id' => 8, 'value' => 4]);
        VanAttribute::create(['van_id' => 2, 'attribute_id' => 9, 'value' => 4]);
        VanAttribute::create(['van_id' => 2, 'attribute_id' => 10, 'value' => 'yes']);
        VanAttribute::create(['van_id' => 2, 'attribute_id' => 11, 'value' => 'LCD (1 x HDMI, 1 x USB)']);
        VanAttribute::create(['van_id' => 2, 'attribute_id' => 12, 'value' => 'yes']);
        VanAttribute::create(['van_id' => 2, 'attribute_id' => 13, 'value' => 'at car part']);
        VanAttribute::create(['van_id' => 2, 'attribute_id' => 14, 'value' => 'yes']);
        VanAttribute::create(['van_id' => 2, 'attribute_id' => 15, 'value' => 'yes']);
        VanAttribute::create(['van_id' => 2, 'attribute_id' => 16, 'value' => 2]);
        VanAttribute::create(['van_id' => 2, 'attribute_id' => 17, 'value' => 'LED']);
        VanAttribute::create(['van_id' => 2, 'attribute_id' => 18, 'value' => '120cm x 90cm']);

        VanAttribute::create(['van_id' => 3, 'attribute_id' => 1, 'value' => '7.45 m']);
        VanAttribute::create(['van_id' => 3, 'attribute_id' => 2, 'value' => '2.95 m']);
        VanAttribute::create(['van_id' => 3, 'attribute_id' => 3, 'value' => '2022']);
        VanAttribute::create(['van_id' => 3, 'attribute_id' => 4, 'value' => '140 hp']);
        VanAttribute::create(['van_id' => 3, 'attribute_id' => 5, 'value' => 'manual']);
        VanAttribute::create(['van_id' => 3, 'attribute_id' => 6, 'value' => 'yes']);
        VanAttribute::create(['van_id' => 3, 'attribute_id' => 7, 'value' => 'yes']);
        VanAttribute::create(['van_id' => 3, 'attribute_id' => 19, 'value' => 'yes']);
        VanAttribute::create(['van_id' => 3, 'attribute_id' => 8, 'value' => 4]);
        VanAttribute::create(['van_id' => 3, 'attribute_id' => 9, 'value' => 5]);
        VanAttribute::create(['van_id' => 3, 'attribute_id' => 10, 'value' => 'yes']);
        VanAttribute::create(['van_id' => 3, 'attribute_id' => 11, 'value' => 'LCD (1 x HDMI, 1 x USB)']);
        VanAttribute::create(['van_id' => 3, 'attribute_id' => 12, 'value' => 'yes']);
        VanAttribute::create(['van_id' => 3, 'attribute_id' => 13, 'value' => 'at car part']);
        VanAttribute::create(['van_id' => 3, 'attribute_id' => 14, 'value' => 'yes']);
        VanAttribute::create(['van_id' => 3, 'attribute_id' => 15, 'value' => 'yes']);
        VanAttribute::create(['van_id' => 3, 'attribute_id' => 16, 'value' => 2]);
        VanAttribute::create(['van_id' => 3, 'attribute_id' => 17, 'value' => 'LED']);
        VanAttribute::create(['van_id' => 3, 'attribute_id' => 18, 'value' => '115cm x 95cm']);

        VanAttribute::create(['van_id' => 4, 'attribute_id' => 1, 'value' => '6.36 m']);
        VanAttribute::create(['van_id' => 4, 'attribute_id' => 2, 'value' => '2.65 m']);
        VanAttribute::create(['van_id' => 4, 'attribute_id' => 3, 'value' => '2022']);
        VanAttribute::create(['van_id' => 4, 'attribute_id' => 4, 'value' => '140 hp']);
        VanAttribute::create(['van_id' => 4, 'attribute_id' => 5, 'value' => 'manual']);
        VanAttribute::create(['van_id' => 4, 'attribute_id' => 6, 'value' => 'yes']);
        VanAttribute::create(['van_id' => 4, 'attribute_id' => 7, 'value' => 'yes']);
        VanAttribute::create(['van_id' => 4, 'attribute_id' => 8, 'value' => 4]);
        VanAttribute::create(['van_id' => 4, 'attribute_id' => 9, 'value' => 4]);
        VanAttribute::create(['van_id' => 4, 'attribute_id' => 10, 'value' => 'yes']);
        VanAttribute::create(['van_id' => 4, 'attribute_id' => 12, 'value' => 'yes']);
        VanAttribute::create(['van_id' => 4, 'attribute_id' => 13, 'value' => 'at car part']);
        VanAttribute::create(['van_id' => 4, 'attribute_id' => 14, 'value' => 'yes']);
        VanAttribute::create(['van_id' => 4, 'attribute_id' => 15, 'value' => 'yes']);
        VanAttribute::create(['van_id' => 4, 'attribute_id' => 16, 'value' => 2]);
        VanAttribute::create(['van_id' => 4, 'attribute_id' => 17, 'value' => 'LED']);
    }
}
