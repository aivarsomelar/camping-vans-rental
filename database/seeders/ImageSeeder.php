<?php

namespace Database\Seeders;

use App\Models\Image;
use Illuminate\Database\Seeder;

class ImageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Image::create(['van_id' => 1, 'path' => 'mobilvetta_kea-i90/mobilvetta-kea-i86_exterieur.jpg']);
        Image::create(['van_id' => 1, 'path' => 'mobilvetta_kea-i90/kea-p86__sb_8236-scaled.jpg']);
        Image::create(['van_id' => 1, 'path' => 'mobilvetta_kea-i90/kea-i-90-2021-scaled-Copy.jpg']);
        Image::create(['van_id' => 1, 'path' => 'mobilvetta_kea-i90/kea-p86__sb_8237-scaled-Copy.jpg']);
        Image::create(['van_id' => 1, 'path' => 'mobilvetta_kea-i90/kea-p86__sb_8239-scaled.jpg']);
        Image::create(['van_id' => 1, 'path' => 'mobilvetta_kea-i90/kea-p86__sb_8241-scaled.jpg']);
        Image::create(['van_id' => 1, 'path' => 'mobilvetta_kea-i90/mbv-kea-i90-003-scaled-Copy.jpg']);
        Image::create(['van_id' => 1, 'path' => 'mobilvetta_kea-i90/mbv-kea-i90-004-scaled.jpg']);
        Image::create(['van_id' => 1, 'path' => 'mobilvetta_kea-i90/mbv-kea-i90-007-scaled-Copy.jpg']);
        Image::create(['van_id' => 1, 'path' => 'mobilvetta_kea-i90/mbv-kea-i90-011-scaled-Copy.jpg']);
        Image::create(['van_id' => 1, 'path' => 'mobilvetta_kea-i90/mbv-kea-i90-017-scaled.jpg']);
        Image::create(['van_id' => 1, 'path' => 'mobilvetta_kea-i90/mbv-kea-i90-020-scaled.jpg']);
        Image::create(['van_id' => 1, 'path' => 'mobilvetta_kea-i90/mbv-kea-i90-023-scaled.jpg']);
        Image::create(['van_id' => 1, 'path' => 'mobilvetta_kea-i90/mbv-kea-i90-027-scaled.jpg']);
        Image::create(['van_id' => 1, 'path' => 'mobilvetta_kea-i90/mbv-kea-i90-033-scaled.jpg']);
        Image::create(['van_id' => 1, 'path' => 'mobilvetta_kea-i90/mbv-kea-i90-044-scaled.jpg']);
        Image::create(['van_id' => 1, 'path' => 'mobilvetta_kea-i90/mbv-kea-i90-048-scaled.jpg']);
        Image::create(['van_id' => 1, 'path' => 'mobilvetta_kea-i90/mbv-kea-i90-058-scaled.jpg']);
        Image::create(['van_id' => 1, 'path' => 'mobilvetta_kea-i90/mbv-kea-i90-059-scaled.jpg']);
        Image::create(['van_id' => 1, 'path' => 'mobilvetta_kea-i90/mbv-kea-i90-060-scaled.jpg']);
        Image::create(['van_id' => 1, 'path' => 'mobilvetta_kea-i90/mbv-kea-i90-063-scaled.jpg']);
        Image::create(['van_id' => 1, 'path' => 'mobilvetta_kea-i90/mbv-kea-i90-084-scaled.jpg']);
        Image::create(['van_id' => 1, 'path' => 'mobilvetta_kea-i90/mbv-kea-i90-099-scaled.jpg']);
        Image::create(['van_id' => 1, 'path' => 'mobilvetta_kea-i90/mbv-kea-i90-109-scaled.jpg']);
        Image::create(['van_id' => 1, 'path' => 'mobilvetta_kea-i90/mbv-kea-i90-114-scaled.jpg']);
        Image::create(['van_id' => 1, 'path' => 'mobilvetta_kea-i90/mbv-kea-i90-128-scaled.jpg']);
        Image::create(['van_id' => 1, 'path' => 'mobilvetta_kea-i90/mobilvetta-kea-i-86-2021-169fullwidth-66549a7b-1712260.jpg']);
        Image::create(['van_id' => 1, 'path' => 'mobilvetta_kea-i90/mobilvetta-kea-i-86-2021-169gallery-4902f136-1712261.jpg']);
        Image::create(['van_id' => 1, 'path' => 'mobilvetta_kea-i90/Mobilvetta_camper_motorhome_K-yacht-80-77-scaled.jpg']);
        Image::create(['van_id' => 1, 'path' => 'mobilvetta_kea-i90/pilt_1.jpg']);
        Image::create(['van_id' => 1, 'path' => 'mobilvetta_kea-i90/pilt_2.jpg']);
        Image::create(['van_id' => 1, 'path' => 'mobilvetta_kea-i90/pilt_3.jpg']);
        Image::create(['van_id' => 1, 'path' => 'mobilvetta_kea-i90/pilt_4.jpg']);
        Image::create(['van_id' => 1, 'path' => 'mobilvetta_kea-i90/pilt_5.jpg']);

        Image::create(['van_id' => 2, 'path' => 'mobilvetta_k-yacht_teknoline_86/k-yacht-teknoline-075-scaled.jpg']);
        Image::create(['van_id' => 2, 'path' => 'mobilvetta_k-yacht_teknoline_86/Capture.JPG']);
        Image::create(['van_id' => 2, 'path' => 'mobilvetta_k-yacht_teknoline_86/k-yacht-teknoline-135-scaled.jpg']);
        Image::create(['van_id' => 2, 'path' => 'mobilvetta_k-yacht_teknoline_86/kyt-90-6-fileminimizer.jpg']);
        Image::create(['van_id' => 2, 'path' => 'mobilvetta_k-yacht_teknoline_86/kyt-90-7-fileminimizer.jpg']);
        Image::create(['van_id' => 2, 'path' => 'mobilvetta_k-yacht_teknoline_86/kyt-90-15-scaled-e1626970464172.jpg']);
        Image::create(['van_id' => 2, 'path' => 'mobilvetta_k-yacht_teknoline_86/kyt-90-16-fileminimizer.jpg']);
        Image::create(['van_id' => 2, 'path' => 'mobilvetta_k-yacht_teknoline_86/kyt-90-26-fileminimizer.jpg']);
        Image::create(['van_id' => 2, 'path' => 'mobilvetta_k-yacht_teknoline_86/kyt-90-28-fileminimizer-Copy.jpg']);
        Image::create(['van_id' => 2, 'path' => 'mobilvetta_k-yacht_teknoline_86/kyt-90-33-fileminimizer.jpg']);
        Image::create(['van_id' => 2, 'path' => 'mobilvetta_k-yacht_teknoline_86/mobilvetta-teknoline-86-031-scaled.jpg']);
        Image::create(['van_id' => 2, 'path' => 'mobilvetta_k-yacht_teknoline_86/mobilvetta-teknoline-86-034-scaled.jpg']);
        Image::create(['van_id' => 2, 'path' => 'mobilvetta_k-yacht_teknoline_86/mobilvetta-teknoline-86-039-scaled.jpg']);
        Image::create(['van_id' => 2, 'path' => 'mobilvetta_k-yacht_teknoline_86/mobilvetta-teknoline-86-040-scaled.jpg']);
        Image::create(['van_id' => 2, 'path' => 'mobilvetta_k-yacht_teknoline_86/mobilvetta-teknoline-86-041-scaled.jpg']);
        Image::create(['van_id' => 2, 'path' => 'mobilvetta_k-yacht_teknoline_86/mobilvetta-teknoline-86-042-scaled.jpg']);
        Image::create(['van_id' => 2, 'path' => 'mobilvetta_k-yacht_teknoline_86/mobilvetta-teknoline-86-043-scaled.jpg']);
        Image::create(['van_id' => 2, 'path' => 'mobilvetta_k-yacht_teknoline_86/mobilvetta-teknoline-86-044-scaled-Copy.jpg']);
        Image::create(['van_id' => 2, 'path' => 'mobilvetta_k-yacht_teknoline_86/mobilvetta-teknoline-86-045-scaled-Copy.jpg']);
        Image::create(['van_id' => 2, 'path' => 'mobilvetta_k-yacht_teknoline_86/mobilvetta-teknoline-86-046-scaled-Copy.jpg']);
        Image::create(['van_id' => 2, 'path' => 'mobilvetta_k-yacht_teknoline_86/mobilvetta-teknoline-86-047-scaled-Copy.jpg']);
        Image::create(['van_id' => 2, 'path' => 'mobilvetta_k-yacht_teknoline_86/mobilvetta-teknoline-90-002-scaled-Copy.jpg']);
        Image::create(['van_id' => 2, 'path' => 'mobilvetta_k-yacht_teknoline_86/mobilvetta-teknoline-90-015-scaled-Copy.jpg']);
        Image::create(['van_id' => 2, 'path' => 'mobilvetta_k-yacht_teknoline_86/mobilvetta-teknoline-90-016-scaled-Copy.jpg']);
        Image::create(['van_id' => 2, 'path' => 'mobilvetta_k-yacht_teknoline_86/mobilvetta-teknoline-90-059-scaled-Copy.jpg']);
        Image::create(['van_id' => 2, 'path' => 'mobilvetta_k-yacht_teknoline_86/mobilvetta-teknoline-90-067-scaled.jpg']);

        Image::create(['van_id' => 3, 'path' => 'mc_louis_mc4_873/mcl800-118.jpg']);
        Image::create(['van_id' => 3, 'path' => 'mc_louis_mc4_873/mc4_373-2020.jpg']);
        Image::create(['van_id' => 3, 'path' => 'mc_louis_mc4_873/mcl800-127.jpg']);
        Image::create(['van_id' => 3, 'path' => 'mc_louis_mc4_873/mclouis-mc4-865g-001-scaled.jpg']);
        Image::create(['van_id' => 3, 'path' => 'mc_louis_mc4_873/mclouis-nevis-873g-010-scaled.jpg']);
        Image::create(['van_id' => 3, 'path' => 'mc_louis_mc4_873/mclouis-nevis-873g-024-scaled.jpg']);
        Image::create(['van_id' => 3, 'path' => 'mc_louis_mc4_873/mclouis-nevis-873g-032-scaled.jpg']);
        Image::create(['van_id' => 3, 'path' => 'mc_louis_mc4_873/mclouis-nevis-873g-053-scaled.jpg']);
        Image::create(['van_id' => 3, 'path' => 'mc_louis_mc4_873/mclouis-nevis-873g-060-scaled.jpg']);
        Image::create(['van_id' => 3, 'path' => 'mc_louis_mc4_873/mclouis-nevis-873g-064-scaled.jpg']);
        Image::create(['van_id' => 3, 'path' => 'mc_louis_mc4_873/mclouis-nevis-873g-066-scaled.jpg']);
        Image::create(['van_id' => 3, 'path' => 'mc_louis_mc4_873/mclouis-nevis-873g-078-scaled.jpg']);
        Image::create(['van_id' => 3, 'path' => 'mc_louis_mc4_873/mclouis__mc4-879__semintegrale__camper-14-scaled.jpg']);
        Image::create(['van_id' => 3, 'path' => 'mc_louis_mc4_873/mclouis__mc4-879__semintegrale__camper-18-small.jpg']);
        Image::create(['van_id' => 3, 'path' => 'mc_louis_mc4_873/mclouis__mc4-879__semintegrale__camper-19-scaled.jpg']);

        Image::create(['van_id' => 4, 'path' => 'bürstner_campeo_c640_fiat/Campeo-1.jpg']);
        Image::create(['van_id' => 4, 'path' => 'bürstner_campeo_c640_fiat/alusplaan.jpg']);
        Image::create(['van_id' => 4, 'path' => 'bürstner_campeo_c640_fiat/Campeo-2.jpg']);
        Image::create(['van_id' => 4, 'path' => 'bürstner_campeo_c640_fiat/Campeo-3.jpg']);
        Image::create(['van_id' => 4, 'path' => 'bürstner_campeo_c640_fiat/Campeo-3-Copy(2).jpg']);
        Image::create(['van_id' => 4, 'path' => 'bürstner_campeo_c640_fiat/Campeo-3-Copy.jpg']);
        Image::create(['van_id' => 4, 'path' => 'bürstner_campeo_c640_fiat/Campeo-4.jpg']);
        Image::create(['van_id' => 4, 'path' => 'bürstner_campeo_c640_fiat/Campeo-5.jpg']);
        Image::create(['van_id' => 4, 'path' => 'bürstner_campeo_c640_fiat/Campeo-6.jpg']);
        Image::create(['van_id' => 4, 'path' => 'bürstner_campeo_c640_fiat/Valispilt.jpg']);
    }
}
