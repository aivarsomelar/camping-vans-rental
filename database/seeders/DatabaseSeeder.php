<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $this->call([
            VansSeeder::class,
            ImageSeeder::class,
            AttributeSeeder::class,
            VanAttributeSeeder::class,
            PriceSeeder::class,
            RolesAndPermissionSeeder::class,
            UserSeeder::class,
        ]);
        // \App\Models\User::factory(10)->create();
    }
}
