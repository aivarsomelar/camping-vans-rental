<?php

namespace Database\Seeders;

use App\Models\Price;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class PriceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Price::create(
            [
                'van_id' => 4,
                'price' => 120,
                'days' => 3,
                'period_start' => Carbon::create(2022, 10, 01),
                'period_end' => Carbon::create(2024, 04, 30),
            ]);
        Price::create(
            [
                'van_id' => 4,
                'price' => 110,
                'days' => 8,
                'period_start' => Carbon::create(2022, 10, 01),
                'period_end' => Carbon::create(2024, 04, 30),
            ]);
        Price::create(
            [
                'van_id' => 4,
                'price' => 90,
                'days' => 15,
                'period_start' => Carbon::create(2022, 10, 01),
                'period_end' => Carbon::create(2024, 04, 30),
            ]);
        Price::create(
            [
                'van_id' => 4,
                'price' => 130,
                'days' => 3,
                'period_start' => Carbon::create(2024, 05, 01),
                'period_end' => Carbon::create(2024, 05, 31),
            ]);
        Price::create(
            [
                'van_id' => 4,
                'price' => 115,
                'days' => 8,
                'period_start' => Carbon::create(2024, 05, 01),
                'period_end' => Carbon::create(2024, 05, 31),
            ]);
        Price::create(
            [
                'van_id' => 4,
                'price' => 95,
                'days' => 15,
                'period_start' => Carbon::create(2024, 05, 01),
                'period_end' => Carbon::create(2024, 05, 31),
            ]);
        Price::create(
            [
                'van_id' => 4,
                'price' => 135,
                'days' => 3,
                'period_start' => Carbon::create(2024, 06, 01),
                'period_end' => Carbon::create(2024, 8, 31),
            ]);
        Price::create(
            [
                'van_id' => 4,
                'price' => 125,
                'days' => 8,
                'period_start' => Carbon::create(2024, 06, 01),
                'period_end' => Carbon::create(2024, 8, 31),
            ]);
        Price::create(
            [
                'van_id' => 4,
                'price' => 100,
                'days' => 15,
                'period_start' => Carbon::create(2024, 06, 01),
                'period_end' => Carbon::create(2024, 8, 31),
            ]);
        Price::create(
            [
                'van_id' => 4,
                'price' => 130,
                'days' => 3,
                'period_start' => Carbon::create(2024, 9, 01),
                'period_end' => Carbon::create(2024, 9, 30),
            ]);
        Price::create(
            [
                'van_id' => 4,
                'price' => 115,
                'days' => 8,
                'period_start' => Carbon::create(2024, 9, 01),
                'period_end' => Carbon::create(2024, 9, 30),
            ]);
        Price::create(
            [
                'van_id' => 4,
                'price' => 95,
                'days' => 15,
                'period_start' => Carbon::create(2024, 9, 01),
                'period_end' => Carbon::create(2024, 9, 30),
            ]);
        Price::create(

            /* van id 3 */
            [
                'van_id' => 3,
                'price' => 115,
                'days' => 3,
                'period_start' => Carbon::create(2022, 10, 01),
                'period_end' => Carbon::create(2024, 04, 30),
            ]);
        Price::create(
            [
                'van_id' => 3,
                'price' => 110,
                'days' => 8,
                'period_start' => Carbon::create(2022, 10, 01),
                'period_end' => Carbon::create(2024, 04, 30),
            ]);
        Price::create(
            [
                'van_id' => 3,
                'price' => 90,
                'days' => 15,
                'period_start' => Carbon::create(2022, 10, 01),
                'period_end' => Carbon::create(2024, 04, 30),
            ]);

        Price::create(
            [
                'van_id' => 3,
                'price' => 120,
                'days' => 3,
                'period_start' => Carbon::create(2024, 05, 01),
                'period_end' => Carbon::create(2024, 05, 31),
            ]);
        Price::create(
            [
                'van_id' => 3,
                'price' => 115,
                'days' => 8,
                'period_start' => Carbon::create(2024, 05, 01),
                'period_end' => Carbon::create(2024, 05, 31),
            ]);
        Price::create(
            [
                'van_id' => 3,
                'price' => 110,
                'days' => 15,
                'period_start' => Carbon::create(2024, 05, 01),
                'period_end' => Carbon::create(2024, 05, 31),
            ]);

        Price::create(
            [
                'van_id' => 3,
                'price' => 145,
                'days' => 3,
                'period_start' => Carbon::create(2024, 06, 01),
                'period_end' => Carbon::create(2024, 8, 31),
            ]);
        Price::create(
            [
                'van_id' => 3,
                'price' => 137,
                'days' => 8,
                'period_start' => Carbon::create(2024, 06, 01),
                'period_end' => Carbon::create(2024, 8, 31),
            ]);
        Price::create(
            [
                'van_id' => 3,
                'price' => 1130,
                'days' => 3,
                'period_start' => Carbon::create(2024, 06, 01),
                'period_end' => Carbon::create(2024, 8, 31),
            ]);

        Price::create(
            [
                'van_id' => 3,
                'price' => 120,
                'days' => 3,
                'period_start' => Carbon::create(2024, 9, 01),
                'period_end' => Carbon::create(2024, 9, 30),
            ]);
        Price::create(
            [
                'van_id' => 3,
                'price' => 115,
                'days' => 8,
                'period_start' => Carbon::create(2024, 9, 01),
                'period_end' => Carbon::create(2024, 9, 30),
            ]);
        Price::create(
            [
                'van_id' => 3,
                'price' => 110,
                'days' => 15,
                'period_start' => Carbon::create(2024, 9, 01),
                'period_end' => Carbon::create(2024, 9, 30),
            ]);

        Price::create(
            /* Van id 1 */
            [
                'van_id' => 1,
                'price' => 190,
                'days' => 3,
                'period_start' => Carbon::create(2022, 10, 01),
                'period_end' => Carbon::create(2024, 04, 30),
            ]);
        Price::create(
            [
                'van_id' => 1,
                'price' => 180,
                'days' => 8,
                'period_start' => Carbon::create(2022, 10, 01),
                'period_end' => Carbon::create(2024, 04, 30),
            ]);
        Price::create(
            [
                'van_id' => 1,
                'price' => 170,
                'days' => 15,
                'period_start' => Carbon::create(2022, 10, 01),
                'period_end' => Carbon::create(2024, 04, 30),
            ]);

        Price::create(
            [
                'van_id' => 1,
                'price' => 200,
                'days' => 3,
                'period_start' => Carbon::create(2024, 05, 01),
                'period_end' => Carbon::create(2024, 05, 31),
            ]);
        Price::create(
            [
                'van_id' => 1,
                'price' => 190,
                'days' => 8,
                'period_start' => Carbon::create(2024, 05, 01),
                'period_end' => Carbon::create(2024, 05, 31),
            ]);
        Price::create(
            [
                'van_id' => 1,
                'price' => 180,
                'days' => 15,
                'period_start' => Carbon::create(2024, 05, 01),
                'period_end' => Carbon::create(2024, 05, 31),
            ]);

        Price::create(
            [
                'van_id' => 1,
                'price' => 230,
                'days' => 3,
                'period_start' => Carbon::create(2024, 06, 01),
                'period_end' => Carbon::create(2024, 8, 31),
            ]);
        Price::create(
            [
                'van_id' => 1,
                'price' => 220,
                'days' => 8,
                'period_start' => Carbon::create(2024, 06, 01),
                'period_end' => Carbon::create(2024, 8, 31),
            ]);
        Price::create(
            [
                'van_id' => 1,
                'price' => 210,
                'days' => 15,
                'period_start' => Carbon::create(2024, 06, 01),
                'period_end' => Carbon::create(2024, 8, 31),
            ]);

        Price::create(
            [
                'van_id' => 1,
                'price' => 200,
                'days' => 3,
                'period_start' => Carbon::create(2024, 9, 01),
                'period_end' => Carbon::create(2024, 9, 30),
            ]);
        Price::create(
            [
                'van_id' => 1,
                'price' => 190,
                'days' => 8,
                'period_start' => Carbon::create(2024, 9, 01),
                'period_end' => Carbon::create(2024, 9, 30),
            ]);
        Price::create(
            [
                'van_id' => 1,
                'price' => 180,
                'days' => 15,
                'period_start' => Carbon::create(2024, 9, 01),
                'period_end' => Carbon::create(2024, 9, 30),
            ]);

        Price::create(
            /* Van id 2 */
            [
                'van_id' => 2,
                'price' => 190,
                'days' => 3,
                'period_start' => Carbon::create(2022, 10, 01),
                'period_end' => Carbon::create(2024, 04, 30),
            ]);
        Price::create(
            [
                'van_id' => 2,
                'price' => 180,
                'days' => 8,
                'period_start' => Carbon::create(2022, 10, 01),
                'period_end' => Carbon::create(2024, 04, 30),
            ]);
        Price::create(
            [
                'van_id' => 2,
                'price' => 170,
                'days' => 15,
                'period_start' => Carbon::create(2022, 10, 01),
                'period_end' => Carbon::create(2024, 04, 30),
            ]);

        Price::create(
            [
                'van_id' => 2,
                'price' => 200,
                'days' => 3,
                'period_start' => Carbon::create(2024, 05, 01),
                'period_end' => Carbon::create(2024, 05, 31),
            ]);
        Price::create(
            [
                'van_id' => 2,
                'price' => 190,
                'days' => 8,
                'period_start' => Carbon::create(2024, 05, 01),
                'period_end' => Carbon::create(2024, 05, 31),
            ]);
        Price::create(
            [
                'van_id' => 2,
                'price' => 180,
                'days' => 15,
                'period_start' => Carbon::create(2024, 05, 01),
                'period_end' => Carbon::create(2024, 05, 31),
            ]);

        Price::create(
            [
                'van_id' => 2,
                'price' => 230,
                'days' => 3,
                'period_start' => Carbon::create(2024, 06, 01),
                'period_end' => Carbon::create(2024, 8, 31),
            ]);
        Price::create(
            [
                'van_id' => 2,
                'price' => 220,
                'days' => 8,
                'period_start' => Carbon::create(2024, 06, 01),
                'period_end' => Carbon::create(2024, 8, 31),
            ]);
        Price::create(
            [
                'van_id' => 2,
                'price' => 210,
                'days' => 15,
                'period_start' => Carbon::create(2024, 06, 01),
                'period_end' => Carbon::create(2024, 8, 31),
            ]);

        Price::create(
            [
                'van_id' => 2,
                'price' => 200,
                'days' => 3,
                'period_start' => Carbon::create(2024, 9, 01),
                'period_end' => Carbon::create(2024, 9, 30),
            ]);
        Price::create(
            [
                'van_id' => 2,
                'price' => 190,
                'days' => 8,
                'period_start' => Carbon::create(2024, 9, 01),
                'period_end' => Carbon::create(2024, 9, 30),
            ]);
        Price::create(
            [
                'van_id' => 2,
                'price' => 170,
                'days' => 15,
                'period_start' => Carbon::create(2024, 9, 01),
                'period_end' => Carbon::create(2024, 9, 30),
            ]);
    }
}
