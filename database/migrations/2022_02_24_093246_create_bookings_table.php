<?php

use App\Models\Customer;
use App\Models\Van;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(Van::class)->constrained();
            $table->foreignIdFor(Customer::class)->constrained();
            $table->float('price')->nullable();
            $table->dateTime('start');
            $table->dateTime('end');
            $table->boolean('is_new')->default(true);
            $table->boolean('is_confirmed')->default(false);
            $table->boolean('is_ended')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('bookings');
    }
};
