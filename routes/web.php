<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\VehicleManagmentController;
use App\Http\Livewire\Admin\Vehicle\Manage\Booking\Show;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [\App\Http\Controllers\VansController::class, 'index'])->name('welcome');
Route::get('/vehicle/{van}', [\App\Http\Controllers\VansController::class, 'show'])->name('vehicle');
Route::view('/rental-conditions', 'about-us')->name('rentalConditions');

Route::get('/register/{token}', [AuthController::class, 'registerForm'])->name('register');
Route::post('/register/store', [AuthController::class, 'sotreUser'])->name('userStore');

Route::middleware(['auth:sanctum', 'verified'])->group(function () {
    Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');

    Route::resource('users', UserController::class);
    Route::resource('vehicleManagment', VehicleManagmentController::class);
    Route::get('vehicleManagment/{van}/booking/{booking}', Show::class)->name('booking.show');
});
